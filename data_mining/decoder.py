import json
import os
from utils import TERRAIN
from utils import HEADERS
from utils import STR
from utils import INT
from utils import split_file
from utils import find_line
from utils import H

INPUT_DIR = 'animals_clean'
OUTPUT_FILE = '../data/animals.json'

DESIRED_PROPERTIES = {
    H.MCHAR_STR : [('cKeeperFoodType', STR), ('cPrefIcon', STR)],\
    H.FCHAR_STR : [('cKeeperFoodType', STR), ('cPrefIcon', STR)],\
    H.MCHAR_INT : [('cNameID', STR), ('cHabitat', STR), ('cLocation', STR),\
                   ('cPurchaseCost', INT), ('cInitialHappiness', INT),\
                   ('cNumberAnimalsMin', INT), ('cNumberAnimalsMax', INT),\
                   ('cAnimalDensity', INT), ('cIsJumper', INT), ('cIsClimber', INT),\
                   ('cBashStrength', INT), ('cTreePref', INT), ('cRockPref', INT),\
                   ('cSpacePref', INT), ('cElevationPref', INT), ('cClimbsCliffs', INT),\
                   ('cAttractiveness', INT), ('cNeedShelter', INT)],\
    H.FCHAR_INT : [('cNameID', STR), ('cHabitat', STR), ('cLocation', STR),\
                   ('cPurchaseCost', INT), ('cInitialHappiness', INT),\
                   ('cNumberAnimalsMin', INT), ('cNumberAnimalsMax', INT),\
                   ('cAnimalDensity', INT), ('cIsJumper', INT), ('cIsClimber', INT),\
                   ('cBashStrength', INT), ('cTreePref', INT), ('cRockPref', INT),\
                   ('cSpacePref', INT), ('cElevationPref', INT), ('cClimbsCliffs', INT),\
                   ('cAttractiveness', INT), ('cNeedShelter', INT)],\
    H.YCHAR_INT : [('cIsJumper', INT), ('cAttractiveness', INT)]
}

def extract(l, t):
    val = l.split('=')[1].split(';')
    if t == STR: val = val[-1].strip()
    elif t == INT: val = int(val[0].strip())

    return val

all_animals = {}

def decode():
    for filename in os.listdir(INPUT_DIR):
        f = open(f'{INPUT_DIR}/{filename}')
        lines = split_file(f)
        properties = dict()
        
        # properties in formats similar to name = value
        for header in DESIRED_PROPERTIES:
            for p, t in DESIRED_PROPERTIES[header]:
                l = find_line(p, lines[header])
                if l < 0: continue # not all properties exist in every file
                if p in properties: p = 'y_' + p
                properties[p] = extract(lines[header][l], t)
        
        # foliage and rocks
        fol_rocks = dict()
        block = lines[H.FOL_ROCKS]
        for i in range(1, len(block), 2):
            fol_rocks[extract(block[i], STR)] = extract(block[i+1], INT)
        properties['foliage_and_rocks'] = fol_rocks

        # terrain
        terrain = dict()
        block = lines[H.COMP_TERRAIN]
        for i in range(1, len(block), 2):
            terrain[TERRAIN[extract(block[i], INT)]] = extract(block[i+1], INT)
        properties['terrain'] = terrain

        all_animals[properties['cNameID']] = properties
        f.close()

    with open(OUTPUT_FILE, "w") as outfile:
        outfile.write(json.dumps(all_animals))