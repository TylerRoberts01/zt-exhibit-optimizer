import json

JSON_FILE = '../data/animals.json'

json_file = open(JSON_FILE)
animal_props = json.load(json_file)
json_file.close()

deletions = [
    # some props have duplicate food types for young
    'y_cKeeperFoodType',
    # some props have duplicate pref items for young
    # moose's is actually incorrect even though it is different
    'y_cPrefIcon'
]

additions = [
    # default young do not jump
    ('y_cIsJumper', 0),
    # mermaids do not have young
    ('y_cAttractiveness', 0),
    # devs misplaced food type for manatee
    ('cKeeperFoodType', 'Seagrass')
]

def clean():
    # delete properties
    for a in animal_props:
        for d in deletions:
            if d in animal_props[a]: del animal_props[a][d]
    # add default values for missing properties
    for a in animal_props:
        for k, v in additions:
            if not k in animal_props[a]:
                animal_props[a][k] = v
    # convert terrain and foliage dicts to lists
    for a in animal_props:
        for p in ['foliage_and_rocks', 'terrain']:
            if not animal_props[a][p] == None:
                v = [[k, animal_props[a][p][k]] for k in animal_props[a][p]]
                v.sort(key = lambda x : -x[1])
                animal_props[a][p] = v

    # commit final cleaning to json
    with open(JSON_FILE, "w") as outfile:
        outfile.write(json.dumps(animal_props))