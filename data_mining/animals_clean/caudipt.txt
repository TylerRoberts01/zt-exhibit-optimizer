[Global]
Class = animals
Type = caudipt
Subtype = 
DefaultSubtype = m
[AnimPath]
m = animals/caudipt/m
f = animals/caudipt/m
y = animals/caudipt/y
[m/Icon]
Icon = animals/caudipt/iccaudip/iccaudip
[Characteristics/Integers]
cExpansionID=1
[Member]
dinosaur
animals
[f/Characteristics/Strings]
[m/Characteristics/Strings]
cKeeperFoodType = carnivore
cGeneralInfoTextName = CAUDIPT_GENERAL
cPlaqueImageName = animals/caudipt/plcaudip/plcaudip
cListImageName = animals/caudipt/lscaudip/lscaudip
cPrefIcon = objects/williams/SE/SE; Williamsonia
[f/Characteristics/Integers]
[m/Characteristics/Integers]
cKeeperFoodType = 6
cPrefIconID = 7095;Williamsonia
cLaysEggs = 1
cEatsEggs = 0
cTimeToHatch = 120
cEggIconZoom = 0
cBoxedIconZoom = -2
cNameID = 5096; Caudipteryx
cHelpID = 5096
cFamily = 5216; Dinosaurs
cGenus = 5146; Caudipteryx
cHabitat = 9405; Tropical Rainforest
cLocation = 9605; China
cEra = 9621; Jurassic
cPurchaseCost = 1500
cInitialHappiness  = 65
cSlowRate = 62
cMediumRate = 80
cFastRate = 95
cFootprintX = 2
cFootprintY = 4
cTestFenceChance = 50
cTestFenceChanceDrop = 5
cMinTestFenceChance = 5
cZapHappinessChange = -25
cBoxFootprintX = 2
cBoxFootprintY = 2
cBoxFootprintZ = 2
cHeliRecovery=1
cBabiesAttack = 0;//Angry Effects//
cPreattack = 1
cCaptivity = 7
cNoFoodChange  = -60
cSickChange = -10	
cOtherAnimalSickChange = -10		
cCrowdHappinessChange = -20
cOtherAnimalAngryChange = 0 
cNumberMinChange = -5
cNumberMaxChange = -10
cAllCrowdedChange = -5
cAngryHabitatChange = -10
cVeryAngryHabitatChange = -30
cDeathChange = -5 
cNotEnoughKeepersChange = -20
cAngryTreeChange = -10
cBabyBornChange = 50;//Happy Effects//
cHappyHabitatChange = 20
cMaxHits  = 225
cMinHits  = 0
cPctHits  = 20
cHungerThreshold  = 50
cHungerIncrement  = 10
cNeededFood = 100
cKeeperFoodUnitsEaten = 10
cHungryHealthChange = 10
cSickChance = 10
cEnergyThreshold = 100 
cEnergyIncrement  = 10	
cMaxEnergy = 100
cCrowd = 30
cKeeperFrequency = 2
cKeeperArrivesChange = 1
cSocial = 1
cHabitatSize = 100
cNumberAnimalsMin = 3
cNumberAnimalsMax = 10
cAnimalDensity = 20
cPctHabitat = 10
cHabitatPreference = 80
cDirtyHabitatRating = 25
cDirtyIncrement = 10
cDirtyThreshold = 100
cReproductionChance = 2
cReproductionInterval = 3600
cBabyToAdult = 1080
cHappyReproduceThreshold = 98
cOffspring = 1
cMatingType = 1
cNoMateChange = -5
cIsJumper = 1
cIsClimber = 0
cIsManEater = 0
cBashStrength = 0
cPrey = 9503;Man
cPrey = 9551;Scientist
cPrey = 9500;Zookeeper
cPrey = 9501;Maintenance Worker
cPrey = 9502;Tour Guide
cPrey = 9552;Marine Specialist
cKeeper = 9551;Keeper
cFlies = 0
cSwims = 0
cEscapedChange = 30
cSickTime = 4
cMimic = 0
cMimicHappyDiff = 0
cOtherFood = 0
cFoodTypes = 0
cTreePref = 6
cRockPref = 2
cSpacePref = 35 
cElevationPref = 10
cTimeDeath = 17280
cDeathChance = 10 
cEatVegetationChance = 0
cDrinkWaterChance = 5
cChaseAnimalChance = 20
cClimbsCliffs = 1
cDinoZoodoo = 1
cSmallZoodoo = 1
cBuildingUseChance = 10
cAttractiveness = 65
cNeedShelter = 0
[y/Characteristics/Integers]
cPrefIconID = 7095
cIconZoom = 0
cBoxedIconZoom = -2
cChaseAnimalChance = 30
cTestFenceChance = 0
cIsJumper = 0
cAttractiveness = 75
cFootprintX = 2
cFootprintY = 2
cSlowRate = 31
cMediumRate = 39
cFastRate = 47
cEggFootprintX = 2
cEggFootprintY = 2
cEggFootprintZ = 2
[m/Characteristics/Mixed]
[cCompatibleAnimals]
v = 5200;Primitive ungulates		
v = -20
v = 5201;Racoon			
v = -10
v = 5202;Bears			
v = -20
v = 5203;Primates			
v = -5
v = 5204;Odd-toed ungulates		
v = -10
v = 5205;Even-toed ungulates			
v = -10
v = 5206;Cat			
v = -20
v = 5207;Canine			
v = -20
v = 5208;Hyena			
v = -20
v = 5210;Marsupial		
v = -5
v = 5211;Bird			
v = -5
v = 5212;Seal			
v = -15
v = 5213;Crocodile		
v = -20
v = 5214;Edentate		    	
v = -5
v = 5215;Pig			
v = -5
v = 5135;Tyrannosaurs
v = -25
v = 5136;Smilodons
v = -10
v = 5137;Ankylosaurs
v = -10
v = 5138;Gallimimuses
v = -5
v = 5139;Iguanodons
v = -10
v = 5140;Lambeosaurs
v = -5
v = 5141;Spinosaurs
v = -25
v = 5142;Styracosaurs
v = -10
v = 5143;Velociraptors
v = -10
v = 5144;Allosaurs
v = -25
v = 5145;Camptosaurs
v = -10
v = 5146;Caudipteryxes
v = -10
v = 5147;Kentrosaurs
v = -10
v = 5148;Plesiosaurs
v = -10
v = 5149;Stegosaurs
v = -10
v = 5150;Apatosaurs
v = -10
v = 5151;Coelophysises
v = -10
v = 5152;Herrerasaurs
v = -25
v = 5153;Plateosaurs
v = -10
v = 5154;Wooly Mammoths
v = -10
v = 5155;Wooly Rhinos
v = -10
v = 5156;Meiolanias
v = -10
v = 5157;Triceratopses
v = -10
v = 5158;Deinosuchus
v = -25
v = 5096;Caudipteryx			
v = 10
[cSuitableObjects]
v = 9410;Non-habitat
v = -10
v = 9405;Tropical rainforest
v = 10
v = 9400;Savannah
v = -5
v = 9401;Grassland
v = -5
v = 9402;Deciduous forest
v = -3
v = 9403;Coniferous forest
v = -3
v = 9407;Highlands
v = -10
v = 9408;Tundra
v = -25
v = 9409;Desert
v = -25
v = 9413;Aquatic
v = -10
v = 9414;Bad-habitat
v = -100
;FOLIAGE AND ROCKS
v = 7085;Fern Bush
v = 15
v = 7095;Williamsonia Tree
v = 20
v = 7015;Elephant Ear Tree
v = 15
v = 7059;Orchid Tree
v = 15
v = 9200;Large Rock 1
v = 5
v = 9201;Large Rock 2
v = 5
v = 9202;Large Rock 3
v = 5
v = 9203;Large Rock 4
v = 5
v = 9204;Large Rock 5
v = 5
v = 9205;Small Rock 1
v = 3
v = 9206;Small Rock 2
v = 3
v = 9207;Small Rock 3
v = 3
v = 9208;Small Rock 4
v = 3
v = 9209;Small Rock 5
v = 3
v = 9226;Rock Formation 5
v = 15
v = 9217;Jungle Back Wall
v = 16
v = 9215;Stone Ruins
v = 10
v = 6062;Poo
v = -10
v = 7082;Thouarsus Cycad
v = 25
v = 7090;Leptocycas
v = 10
;SHELTERS
v = 8100;Rock Cave 1
v = 25
v = 8107;Concrete House 1
v = 15
v = 8108;Concrete House 2
v = 18
v = 8109;Concrete House 3
v = 20
v = 8110;Wood House 1
v = 15
v = 8111;Wood House 2
v = 18
v = 8112;Wood House 3
v = 20
[cCompatibleTerrain]
v = 16
v = -5
v = 2
v = -5
v = 4
v = 50
v = 6
v = -5
v = 9
v = 30
v = 10
v = -20
v = 15
v = -15
v = 14
v = -15
v = 0
v = 20
v = 11
v = -5
[m/Animations]
;From stand
idle = stand
stand = stand
bite = bite
display = display
eat = eat
head_ground = headgrnd
jerk = jerk
kick = kick
look_left = lookl
look_right = lookr
neck_sway = necksway
shake = shake
sit = sit
sit_idle = sitidle
sit_lie = sitlie
stretch = stretch
tail_bob = tailbob
wave = wave
test_fence = fencezap
;Lying Down
sleep = sleep
;Movement
walk = walk
run = run
march = march
;Box and Dustball animations
box_idle = objects/dinobox2/idle/idle.ani
box_used = objects/dinobox2/used/used.ani
dustball = objects/dustcl/small/small.ani
egg_idle = objects/e-caudip/idle/idle.ani
egg_hatch = objects/e-caudip/used/used.ani
egg_break = objects/e-caudip/break/break.ani
[y/Animations]
;From stand
idle = stand
stand = stand
bite = bite
display = display
eat = eat
head_ground = headgrnd
jerk = jerk
kick = kick
look_left = lookl
look_right = lookr
neck_sway = necksway
shake = shake
sit = sit
sit_idle = sitidle
sit_lie = sitlie
stretch = stretch
tail_bob = tailbob
wave = wave
;Lying Down
sleep = sleep
;Movement
walk = walk
run = run
march = march
;Box and Dustball animations
box_idle = objects/dinobox2/idle/idle.ani
box_used = objects/dinobox2/used/used.ani
dustball = objects/dustcl/small/small.ani
egg_idle = objects/e-caudip/idle/idle.ani
egg_hatch = objects/e-caudip/used/used.ani
egg_break = objects/e-caudip/break/break.ani
[Sounds]
;Male sounds
roar = animals/caudipt/roar.wav
roar = 300
roar2 = animals/caudipt/roar2.wav
roar2 = 300
zap = sounds/electric.wav
zap = 300
;dustball, place and pickup
dustball = animals/caudipt/roar2.wav
dustball = 300
placesound = animals/caudipt/roar.wav
placesound = 1500
pickupsound = animals/caudipt/roar2.wav
pickupsound = 1500
hatch = sounds/egghatch.wav
hatch= 100
;Caudipteryx Male Behavior Set Probabilities
[m\BehaviorSet\bHappy]
f = fPlaySetProb(bIdle1,20,bHappy1,50,bHappy2,30)
[m\BehaviorSet\bFriendly]
f = fPlaySetProb(bHappy3,70,bIdle2,30)
[m\BehaviorSet\bCalm]
f = fPlaySetProb(bCalm1,50,bIdle1,30,bIdle2,20)
[m\BehaviorSet\bNeutral]
f = fPlaySetProb(bIdle1,40,bIdle2,40,bNeutral1,20)
[m\BehaviorSet\bUneasy]
f = fPlaySetProb(bUneasy1,35,bNoise1,15,bIdle2,15,bUneasy2,35)
[m\BehaviorSet\bTense]
f = fPlaySetProb(bTense1,60,bAngry3,20,bNoise1,20)
[m\BehaviorSet\bAngry]
f = fPlaySetProb(bAngry1,50,bAngry2,50)
[m\BehaviorSet\bEscaped]
f = fPlaySetProb(bEscaped1,70,bNoise2,30)
;[m\BehaviorSet\bTest]
;f = fPlaySetProb(bWalk,50,bRun,50)
;Caudipteryx Male behaviors
[m\BehaviorSet\bHappy1]
f = fPlay(stand)
f = fPlay(shake)
f = fPlayPingPong(look_left)
f = fPlayWithSound(display,roar2)
f = fPlay(stand)
f = fWalk(0,0)
f = fPlay(stand)
[m\BehaviorSet\bHappy2]
f = fPlay(stand)
f = fPlay(tail_bob)
f = fPlay(stand)
f = fMove(0,0,march,80)
f = fPlayWithSound(display,roar2)
f = fPlay(shake)
f = fPlay(stand)
[m\BehaviorSet\bHappy3]
f = fPlay(stand)
f = fPlayWithSound(display,roar2)
f = fRun(0,0)
f = fPlay(tail_bob)
f = fPlayPingPong(look_right)
f = fPlay(stand)
[m\BehaviorSet\bCalm1]
f = fPlay(stand)
f = fPlay(stretch)
f = fPlayTime(stand,5)
f = fPlay(neck_sway)
f = fPlayPingPong(look_left)
f = fPlay(shake)
f = fPlay(stand)
[m\BehaviorSet\bUneasy1]
f = fPlay(stand)
f = fPlayPingPong(look_left)
f = fPlayPingPong(look_right)
f = fPlay(shake)
f = fPlay(stand)
[m\BehaviorSet\bUneasy2]
f = fPlay(stand)
f = fRun(0,0)
f = fPlay(shake)
f = fRun(0,0)
f = fPlay(jerk)
f = fPlay(stand)
[m\BehaviorSet\bTense1]
f = fPlay(stand)
f = fPlay(tail_bob)
f = fPlay(stretch)
f = fPlayPingPong(look_right)
f = fPlay(jerk)
f = fPlay(stand)
[m\BehaviorSet\bAngry1]
f = fPlay(stand)
f = fPlay(kick)
f = fPlayWithSound(bite,roar)
f = fPlay(shake)
f = fPlayWithSound(bite,roar)
f = fPlayWithSound(display,roar2)
f = fPlay(kick)
f = fPlay(stand)
[m\BehaviorSet\bAngry2]
f = fPlay(stand)
f = fPlayWithSound(bite,roar)
f = fPlay(shake)
f = fRun(0,0)
f = fPlayWithSound(bite,roar)
f = fRun(0,0)
f = fPlay(kick)
f = fPlay(jerk)
f = fPlayWithSound(display,roar2)
f = fPlay(stand)
[m\BehaviorSet\bAngry3]
f = fPlay(stand)
f = fPlayWithSound(bite,roar)
f = fPlayWithSound(display,roar2)
f = fPlayWithSound(kick,roar)
f = fRun(0,0)
f = fPlay(stand)
[m\BehaviorSet\bNoise1]
f = fPlay(stand)
f = fPlayWithSound(bite,roar)
f = fPlay(stand)
[m\BehaviorSet\bNoise2]
f = fPlay(stand)
f = fPlayWithSound(display,roar2)
f = fPlay(stand)
[m\BehaviorSet\bIdle1]
f = fPlay(stand)
f = fWalk(0,0)
f = fPlay(tail_bob)
f = fMove(0,0,march,80)
f = fPlay(stand)
[m\BehaviorSet\bIdle2]
f = fPlay(stand)
f = fPlayPingPong(look_left)
f = fPlay(neck_sway)
f = fMove(0,0,march,80)
f = fPlay(shake)
f = fPlay(stand)
[m\BehaviorSet\bIdle3]
f = fPlay(stand)
f = fPlay(jerk)
f = fWalk(0,0)
f = fPlay(tail_bob)
f = fPlay(stand)
[m\BehaviorSet\bNeutral1]
f = fPlay(stand)
f = fPlay(tail_bob)
f = fPlay(stand)
f = fMove(0,0,march,80)
f = fPlay(sit)
f = fPlayTime(sit_idle,10)
f = fPlayReverse(sit)
f = fMove(0,0,march,80)
f = fPlay(stand)
[m\BehaviorSet\bSleep]
f = fPlay(stand)
f = fPlay(sit)
f = fPlayTime(sit_idle,5)
f = fPlay(sit_lie)
f = fPlay(sleep)
f = fPlayTime(sleep,20)
f = fPlayReverse(sit_lie)
f = fPlayReverse(sit)
f = fPlay(stand)
[m\BehaviorSet\bDie]
f = fPlay(stand)
f = fPlay(sit)
f = fPlay(sit_lie)
f = fPlay(sleep)
f = fPlayTime(sleep,5)
f = fDie()
[m\BehaviorSet\bFindKeeperFood]
f = fWalk(keeperfood,0)
f = fPlaySet(bEat)
[m\BehaviorSet\bFindOtherFood]
f = fWalk(otherfood,0)
f = fPlaySet(bEat)
[m\BehaviorSet\bEat]
f = fFaceTowardFood()
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,9)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[m\BehaviorSet\bSick]
f = fPlay(stand)
f = fPlay(sit)
f = fPlay(sit_lie)
f = fPlay(sleep)
f = fPlayTime(sleep,20)
f = fPlayReverse(sit_lie)
f = fPlayReverse(sit)
f = fPlay(stand)
[m\BehaviorSet\bEscaped1]
f = fRun(0,0)
[m\BehaviorSet\bChasePrey]
f = fRun(prey,0)
[m\BehaviorSet\bPreattack]
f = fPlay(tail_bob)
f = fPlayWithSound(bite,roar)
[m\BehaviorSet\bDefense]
f = fPlay(kick)
f = fPlayWithSound(display,roar2)
[m\BehaviorSet\bTestFence]
f = fPlayWithSound(test_fence,zap)
[m\BehaviorSet\bRunFromPredator]
f = fRun(0,0)
[m\BehaviorSet\bCaughtGuest]
; the time of animation below should match bCaughtBy in guest
f = fPlayTime(stand,4)
[m\BehaviorSet\bCaughtPrey]
f = fDustBall()
f = fPlaySet(bAfterCaught)
[m\BehaviorSet\bAfterCaught]
f = fPlay(stand)
f = fPlay(shake)
f = fPlay(jerk)
f = fPlay(stand)
[m\BehaviorSet\bCaught]
f = fFollow(keeper)
[m\BehaviorSet\bWalk]
f = fWalk(0,0)
[m\BehaviorSet\bRun]
f = fRun(0,0)
[m\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
;Caudipteryx Young Behavior Set Probabilities
[y\BehaviorSet\bHappy]
f = fPlaySetProb(bIdle1,20,bHappy1,50,bHappy2,30)
[y\BehaviorSet\bFriendly]
f = fPlaySetProb(bHappy3,70,bIdle2,30)
[y\BehaviorSet\bCalm]
f = fPlaySetProb(bCalm1,50,bIdle1,30,bIdle2,20)
[y\BehaviorSet\bNeutral]
f = fPlaySetProb(bIdle1,40,bIdle2,40,bNeutral1,20)
[y\BehaviorSet\bUneasy]
f = fPlaySetProb(bUneasy1,35,bNoise1,15,bIdle2,15,bUneasy2,35)
[y\BehaviorSet\bTense]
f = fPlaySetProb(bTense1,60,bAngry3,20,bNoise1,20)
[y\BehaviorSet\bAngry]
f = fPlaySetProb(bAngry1,50,bAngry2,50)
[y\BehaviorSet\bEscaped]
f = fPlaySetProb(bEscaped1,70,bNoise2,30)
;Caudipteryx Young behaviors
[y\BehaviorSet\bHappy1]
f = fPlay(stand)
f = fPlay(shake)
f = fPlayPingPong(look_left)
f = fPlayWithSound(display,roar2)
f = fPlay(stand)
f = fRun(0,0)
f = fPlay(stand)
[y\BehaviorSet\bHappy2]
f = fPlay(stand)
f = fPlay(tail_bob)
f = fPlayWithSound(display,roar2)
f = fPlay(stand)
f = fPlay(shake)
f = fRun(0,0)
f = fPlay(stand)
[y\BehaviorSet\bHappy3]
f = fPlay(stand)
f = fPlayPingPong(look_right)
f = fPlayWithSound(display,roar2)
f = fPlay(tail_bob)
f = fRun(0,0)
f = fPlay(stand)
[y\BehaviorSet\bCalm1]
f = fPlay(stand)
f = fPlay(stretch)
f = fPlay(shake)
f = fPlayTime(stand,5)
f = fPlayPingPong(look_left)
f = fPlay(neck_sway)
f = fPlay(stand)
[y\BehaviorSet\bUneasy1]
f = fPlay(stand)
f = fPlayPingPong(look_right)
f = fPlay(shake)
f = fPlayPingPong(look_left)
f = fPlay(stand)
[y\BehaviorSet\bUneasy2]
f = fPlay(stand)
f = fRun(0,0)
f = fPlay(jerk)
f = fPlay(shake)
f = fRun(0,0)
f = fPlay(stand)
[y\BehaviorSet\bTense1]
f = fPlay(stand)
f = fPlay(tail_bob)
f = fPlayPingPong(look_right)
f = fPlay(jerk)
f = fPlay(stretch)
f = fPlay(stand)
[y\BehaviorSet\bAngry1]
f = fPlay(stand)
f = fPlay(kick)
f = fPlayWithSound(display,roar2)
f = fPlayWithSound(bite,roar)
f = fPlay(shake)
f = fPlay(kick)
f = fPlayWithSound(bite,roar)
f = fPlay(stand)
[y\BehaviorSet\bAngry2]
f = fPlay(stand)
f = fPlayWithSound(bite,roar)
f = fRun(0,0)
f = fPlay(shake)
f = fPlayWithSound(bite,roar)
f = fPlay(jerk)
f = fRun(0,0)
f = fPlayWithSound(display,roar2)
f = fPlay(kick)
f = fPlay(stand)
[y\BehaviorSet\bAngry3]
f = fPlay(stand)
f = fPlayWithSound(display,roar2)
f = fPlayWithSound(bite,roar)
f = fRun(0,0)
f = fPlayWithSound(kick,roar)
f = fPlay(stand)
[y\BehaviorSet\bNoise1]
f = fPlay(stand)
f = fPlayWithSound(display,roar2)
f = fPlay(stand)
[y\BehaviorSet\bNoise2]
f = fPlay(stand)
f = fPlayWithSound(bite,roar)
f = fPlay(stand)
[y\BehaviorSet\bIdle1]
f = fPlay(stand)
f = fMove(0,0,march,80)
f = fPlay(tail_bob)
f = fWalk(0,0)
f = fPlay(stand)
[y\BehaviorSet\bIdle2]
f = fPlay(stand)
f = fPlay(neck_sway)
f = fPlay(shake)
f = fMove(0,0,march,80)
f = fPlayPingPong(look_left)
f = fPlay(stand)
[y\BehaviorSet\bIdle3]
f = fPlay(stand)
f = fPlay(tail_bob)
f = fPlay(jerk)
f = fWalk(0,0)
f = fPlay(stand)
[y\BehaviorSet\bNeutral1]
f = fPlay(stand)
f = fMove(0,0,march,80)
f = fPlay(stand)
f = fPlay(sit)
f = fPlayTime(sit_idle,10)
f = fPlayReverse(sit)
f = fMove(0,0,march,80)
f = fPlay(tail_bob)
f = fPlay(stand)
[y\BehaviorSet\bSleep]
f = fPlay(stand)
f = fPlay(sit)
f = fPlayTime(sit_idle,5)
f = fPlay(sit_lie)
f = fPlay(sleep)
f = fPlayTime(sleep,20)
f = fPlayReverse(sit_lie)
f = fPlayReverse(sit)
f = fPlay(stand)
[y\BehaviorSet\bDie]
f = fPlay(stand)
f = fPlay(sit)
f = fPlay(sit_lie)
f = fPlay(sleep)
f = fPlayTime(sleep,5)
f = fDie()
[y\BehaviorSet\bFindKeeperFood]
f = fWalk(keeperfood,0)
f = fPlaySet(bEat)
[y\BehaviorSet\bFindOtherFood]
f = fWalk(otherfood,0)
f = fPlaySet(bEat)
[y\BehaviorSet\bEat]
f = fFaceTowardFood()
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,9)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[y\BehaviorSet\bSick]
f = fPlay(stand)
f = fPlay(sit)
f = fPlay(sit_lie)
f = fPlay(sleep)
f = fPlayTime(sleep,20)
f = fPlayReverse(sit_lie)
f = fPlayReverse(sit)
f = fPlay(stand)
[y\BehaviorSet\bEscaped1]
f = fRun(0,0)
[y\BehaviorSet\bChasePrey]
f = fRun(prey,0)
[y\BehaviorSet\bPreattack]
f = fPlayWithSound(bite,roar)
f = fPlay(tail_bob)
[y\BehaviorSet\bDefense]
f = fPlayWithSound(display,roar2)
f = fPlay(kick)
[y\BehaviorSet\bRunFromPredator]
f = fRun(0,0)
[y\BehaviorSet\bCaughtGuest]
; the time of animation below should match bCaughtBy in guest
f = fPlayTime(stand,4)
[y\BehaviorSet\bCaughtPrey]
f = fDustBall()
f = fPlaySet(bAfterCaught)
[y\BehaviorSet\bAfterCaught]
f = fPlay(stand)
f = fPlay(jerk)
f = fPlay(shake)
f = fPlay(stand)
[y\BehaviorSet\bCaught]
f = fFollow(keeper)
[y\BehaviorSet\bWalk]
f = fWalk(0,0)
[y\BehaviorSet\bRun]
f = fRun(0,0)
[y\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
[y\BehaviorSet\bHatch]
f = fPlayWithSound(egg_hatch,hatch)
f = fPlay(idle)
f = fHatch()
[y\BehaviorSet\bEatenEgg]
f = fPlayStopAtEnd(egg_break)
[AmbientAnims]
;a = -100
;a = 100
;b = bTest
a = 81
a = 100
b = bHappy
a = 61
a = 80
b = bFriendly
a = 21
a = 60
b = bCalm
a = -20
a = 20
b = bNeutral
a = -60
a = -21
b = bUneasy
a = -80
a = -61
b = bTense
a = -100
a = -81
b = bAngry
;Test BehaviorSet Probabilities to test bHappy1
;[m\BehaviorSet\bHappy]
;f = fPlaySetProb(bHappy1,100)
;Test BehaviorSet Probabilities for testing sound
;[m\BehaviorSet\bSoundtest]
;f = fPlaySetProb(bSoundtest1,100)
[m\BehaviorSet\bSoundtest1]
;f = fPlayWithSound(hunt_roar,huntror)
;f = fPlayWithSound(roar,roar)
;f = fPlayWithSound(roar_up,rorup)
;f = fPlayWithSound(roar_up_left,rorupl)
;f = fPlayWithSound(roar_up_right,rorupr)
;f = fPlayWithSound(roar_down,rordwn)
;f = fPlayWithSound(roar_down_left,rordownl)
;f = fPlayWithSound(roar_down_right,rordownr)
;f = fPlayWithSound(stomp_left,stompl)
;f = fPlayWithSound(stomp_right,stompr)
;f = fPlayWithSound(snort,snort)
;No longer needed
;[m\BehaviorSet\bBaby1]
;f = fPlay(stand)
;f = fWalk(mother,0)
;No longer needed
;[m\BehaviorSet\bBaby2]
;f = fPlay(stand)
;f = fWalk(mother,0)
;No longer needed
;[m\BehaviorSet\bAngryKeeper]
;f = fPlay(stand)
;No longer needed
;[m\BehaviorSet\bBabyActions]
;f = fPlaySetProb(bBaby1,40,bBaby2,40,bIdle1,20)
;Caudipteryx does not play with toys
;[m\BehaviorSet\bPlayToys]
;f = fPlaySetProb(bToy3,20,bSpray1,20,bLog1,20,bToy1,20,bToy2,20)
;Caudipteryx does not play with toys
;[m\BehaviorSet\bPlayWithToyHappy]
;f = fPlaySetProb(bPlayToys,50,bIdle1,20,bHappy2,20,bNoise1,10)
;Caudipteryx does not play with toys
;[m\BehaviorSet\bPlayWithToyFriendly]
;f = fPlaySetProb(bPlayToy,50,bIdle2,30,bNoise2,20)
;Test BehaviorSet Probabilities to test bHappy1
;[y\BehaviorSet\bHappy]
;f = fPlaySetProb(bHappy1,100)
;Test BehaviorSet Probabilities for testing sound
;[y\BehaviorSet\bSoundtest]
;f = fPlaySetProb(bSoundtest1,100)
;No longer needed
;[y\BehaviorSet\bBabyActions]
;f = fPlaySetProb(bBaby1,40,bBaby2,40,bIdle1,20)
;Caudipteryx does not play with toys
;[y\BehaviorSet\bPlayToys]
;f = fPlaySetProb(bToy3,20,bSpray1,20,bLog1,20,bToy1,20,bToy2,20)
;Caudipteryx does not play with toys
;[y\BehaviorSet\bPlayWithToyHappy]
;f = fPlaySetProb(bPlayToys,50,bIdle1,20,bHappy2,20,bNoise1,10)
;Caudipteryx does not play with toys
;[y\BehaviorSet\bPlayWithToyFriendly]
;f = fPlaySetProb(bPlayToy,50,bIdle2,30,bNoise2,20)
;[y\BehaviorSet\bSoundtest1]
;f = fPlayWithSound(hunt_roar,huntror)
;f = fPlayWithSound(roar,roar)
;f = fPlayWithSound(roar_up,rorup)
;f = fPlayWithSound(roar_up_left,rorupl)
;f = fPlayWithSound(roar_up_right,rorupr)
;f = fPlayWithSound(roar_down,rordwn)
;f = fPlayWithSound(roar_down_left,rordownl)
;f = fPlayWithSound(roar_down_right,rordownr)
;f = fPlayWithSound(stomp_left,stompl)
;f = fPlayWithSound(stomp_right,stompr)
;f = fPlayWithSound(snort,snort)
;No longer needed
;[y\BehaviorSet\bBored1]
;f = fPlayTime(stand,10)
;No longer needed
;[y\BehaviorSet\bBored2]
;f = fPlayTime(stand,10)
;f = fPlay(yawn)
;f = fPlayTime(stand,5)
;No longer needed
;[y\BehaviorSet\bAngryKeeper]
;f = fPlay(stand)
;No longer needed
;[y\BehaviorSet\bBaby1]
;f = fPlay(stand)
;f = fWalk(mother,0)
;No longer needed
;[y\BehaviorSet\bBaby2]
;f = fPlay(stand)
;f = fWalk(mother,0)