[Global]
Class = animals
Type = gsquid
Subtype = 
DefaultSubtype = m
[AnimPath]
m = animals/gsquid/m
f = animals/gsquid/m
y = animals/gsquid/y
[m/Icon]
Icon = animals/gsquid/icgsquid/icgsquid
[Characteristics/Integers]
; shadows for this unit
cUsesRealShadows = 1
cHasShadowImages = 1
cHasUnderwaterSection = 1
cExpansionID=2
[Member]
animals
aqua
[f/Characteristics/Strings]
[m/Characteristics/Strings]
cKeeperFoodType = carnivore
cGeneralInfoTextName = GSQUID_GENERAL
cPlaqueImageName = animals/gsquid/plgsquid/plgsquid
cListImageName = animals/gsquid/lsgsquid/lsgsquid
cPrefIcon = objects/tubeworm/SE/SE;Tube Worm
[f/Characteristics/Integers]
[m/Characteristics/Integers]
cKeeperFoodType = 10
cPrefIconID = 7420;Tube Worm
cNameID = 5421; Giant Squid
cHelpID = 5421
cFamily = 5228; Architeuthidae
cGenus = 5180; Giant Squids
cHabitat = 9413; Aquatic
cLocation = 9633; Many
cPurchaseCost = 6000
cInitialHappiness  = 65
cSlowRate = 50
cMediumRate = 75
cFastRate = 100
cFootprintX = 2
cFootprintY = 6
cBoxedIconZoom = -2
cIconZoom = -2
cBabiesAttack = 0;//ANGRY EFFECTS//
cCaptivity = 10
cNoFoodChange  = -50
cSickChange = -12	
cOtherAnimalSickChange = -10		
cCrowdHappinessChange = -5
cOtherAnimalAngryChange = 0
cNumberMinChange = -5
cNumberMaxChange = -5
cAllCrowdedChange = -5
cAngryHabitatChange = -5
cVeryAngryHabitatChange = -30
cDeathChange = -5 
cAngryTreeChange = -5
cNotEnoughKeepersChange = -20
cBabyBornChange = 30;//HAPPY EFFECTS//
cHappyHabitatChange = 20
cMaxHits  = 400
cMinHits  = 0
cPctHits  = 20
cHungerThreshold  = 50
cHungerIncrement  = 11
cNeededFood = 100
cKeeperFoodUnitsEaten = 25
cHungryHealthChange = 10
cSickChance = 5
cEnergyThreshold = 100 
cEnergyIncrement  = 5	
cMaxEnergy = 100
cCrowd = 20
cKeeperFrequency = 2
cSocial = 0
cHabitatSize = 100
cNumberAnimalsMin = 1
cNumberAnimalsMax = 2
cAnimalDensity = 50
cReproductionChance = 1
cReproductionInterval = 3240
cBabyToAdult = 1440
cHappyReproduceThreshold = 98
cOffspring = 2
cMatingType = 0
cNoMateChange = 0
cIsJumper = 0
cIsClimber = 0
cBashStrength = 0
cFlies = 0
cSwims = 0
cUnderwater = 1; Underwater characteristics
cOnlyUnderwater = 1
cPctHabitat = 10
cHabitatPreference = 70
cEscapedChange = 50
cSickTime = 4
cMimic = 10
cMimicHappyDiff = 50
cOtherFood = 0
cFoodTypes = 0
cTreePref = 9
cRockPref = 4
cSpacePref = 80 
cElevationPref = 0
cDepthMin = 13
cDepthMax = 29
cDepthChange = -10
cSalinityChange = -10
cSalinityHealthChange = -20
cDepth = 2
cAvoidEdges = 1; Avoid tank walls
cDirtyHabitatRating = 15
cDirtyIncrement = 10
cDirtyThreshold = 100
cPooWaterImpact = 5 
cMurkyWaterChange = -5
cMurkyWaterHealthChange = -5
cMurkyWaterThreshold = 60
cVeryMurkyWaterChange = -10
cVeryMurkyWaterHealthChange = -10
cVeryMurkyWaterThreshold = 20
cExtremelyMurkyWaterChange = -15
cExtremelyMurkyWaterHealthChange = -15
cExtremelyMurkyWaterThreshold= 1
cTimeDeath = 12960
cDeathChance = 10
cEatVegetationChance = 0
cDrinkWaterChance = 0
cChaseAnimalChance = 0
cClimbsCliffs = 0
cResetPreyPosition = 1
cBuildingUseChance = 30
cPrey = 9503;Man
cPrey = 5422;Bluefin Tuna
cPrey = 5412;Moray Eel
cPrey = 5414;Sea Otter
cPrey = 5419;Sea Turtle
cPrey = 5423;Barracuda
cPrey = 5415;Octopus
cPrey = 5403;Arctic Lion's Mane Jellyfish
cAttractiveness = 80
cNeedShelter = 1
cKeeper = 9552;Keeper is marine specialist
[y/Characteristics/Integers]
cIsJumper = 0
cFootprintX = 2
cFootprintY = 4
cAttractiveness = 98
[m/Characteristics/Mixed]
[cCompatibleAnimals]
v = 5200;Primitive ungulates
v = -20
v = 5201;Racoon
v = -10
v = 5202;Bears
v = -20
v = 5203;Primates
v = -5
v = 5204;Odd-toed ungulates
v = -10
v = 5205;Even-toed ungulates
v = -10
v = 5206;Cat
v = -20
v = 5207;Canine
v = -20
v = 5208;Hyena
v = -20
v = 5210;Marsupial
v = -5
v = 5211;Bird
v = -5
v = 5212;Seal
v = -15
v = 5213;Crocodile
v = -20
v = 5214;Edentate
v = -5
v = 5215;Pig
v = -5
v = 5135;Tyrannosaurs
v = -10
v = 5136;Smilodons
v = -10
v = 5137;Ankylosaurs
v = -10
v = 5138;Gallimimuses
v = -10
v = 5139;Iguanodons
v = -10
v = 5140;Lambeosaurs
v = -10
v = 5141;Spinosaurs
v = -25
v = 5142;Styracosaurs
v = -10
v = 5143;Velociraptors
v = -10
v = 5144;Allosaurs
v = -10
v = 5145;Camptosaurs
v = -10
v = 5146;Caudipteryxes
v = -10
v = 5147;Kentrosaurs
v = -10
v = 5148;Plesiosaurs
v = -10
v = 5149;Stegosaurs
v = -10
v = 5150;Apatosaurs
v = -10
v = 5151;Coelophysises
v = -10
v = 5152;Herrerasaurs
v = -10
v = 5153;Plateosaurs
v = -10
v = 5154;Wooly Mammoths
v = -10
v = 5155;Wooly Rhinos
v = -10
v = 5156;Meiolanias
v = -10
v = 5157;Triceratopses
v = -10
v = 5158;Deinosuchus
v = -25
v = 5159;Orcas
v = -10
v = 5160;Great White Sharks
v = -20
v = 5161;Bottlenose Dolphins
v = -10
v = 5162;Jellyfishes
v = -10
v = 5163;Northern Elephant Seals
v = -10
v = 5164;Humpback Whales
v = -10
v = 5165;Sperm Whales
v = -10
v = 5166;Narwhals
v = -10
v = 5167;Harbor Porpoises
v = -10
v = 5168;Hammerhead Sharks
v = -10
v = 5169;Tiger Sharks
v = -10
v = 5170;Mako Sharks
v = -20
v = 5171;Moray Eels
v = -10
v = 5172;Belugas
v = -10
v = 5173;Sea Otters
v = -10
v = 5174;Octopuses
v = -10
v = 5175;Manatees
v = -10
v = 5176;Walruses
v = -10
v = 5177;Manta Rays
v = -20
v = 5178;Sea Turtles
v = -10
v = 5179;Swordfishes
v = -10
v = 5180;Giant Squids
v = -20
v = 5181;Bluefin Tuna
v = -10
v = 5182;Baracudas
v = -10
v = 5183;Mermaids
v = 10
v = 5185;Whale Sharks
v = -10
v = 5187;Bowhead Whales
v = -10
v = 5189;Sawfish
v = -10
v = 5421;Giant Squid
v = 20
v = 5415;Octopus
v = 15
v = 5403;Arctic Lion Mane's Jellyfish		
v = 15
[cSuitableObjects]
v = 9410;Non-habitat
v = -10
v = 9405;Tropical rainforest
v = -25
v = 9400;Savannah
v = -25
v = 9401;Grassland
v = -25
v = 9402;Deciduous forest
v = -25
v = 9403;Coniferous forest
v = -25
v = 9407;Highlands
v = -25
v = 9408;Tundra
v = -10
v = 9409;Desert
v = -25
v = 9413;Aquatic
v = 0
v = 9414;Bad-habitat
v = -100
;FOLIAGE AND ROCKS
v = 7061;Water Reed
v = 0
v = 7062;Water Lily
v = 0
v = 7083;Bald Cypress
v = 0
v = 7088;Horse Tail
v = 0
v = 7400;Sea Anemone
v = 6
v = 7401;Barnacles
v = 6
v = 7402;Beach Grass
v = 0
v = 7403;Brittle Sea Star
v = 6
v = 7404;Clambed
v = 6
v = 7405;Orange Cup Coral
v = 6
v = 7406;Divercate Tree Coral
v = 0
v = 7407;Feather Duster Worm
v = 0
v = 7408;Fire Cup Coral
v = 0
v = 7409;Kelp
v = 6
v = 7410;Sea Lettuce
v = 6
v = 7411;Red Gorgonian
v = 3
v = 7412;Sargassum
v = 3
v = 7413;Sand Dollar
v = -2
v = 7414;Sea Cucumber
v = 3
v = 7415;Sea Star
v = 6
v = 7416;Sea Weed
v = 6
v = 7417;Sea Grass
v = 0
v = 7418;Sea Sponge
v = -2
v = 7419;Stovepipe Sponge
v = 0
v = 7420;Tubeworm
v = 8
v = 7421;Purple Sea Urchin
v = -2
v = 9234;Aquatic rock
v = 7
v = 9235;Small Oceanfloor Rock
v = 6
v = 9236;Medium Coral Formation
v = 6
v = 9237;Large Oceanfloor Rock
v = 6
v = 9238;Medium Oceanfloor Rock
v = 6
v = 9239;Large Coral Formation
v = 6
v = 9240;Iceberg
v = 6
v = 9241;Isle Rock
v = 6
v = 6062;Poo
v = -5
v = 6466;Fake Clam
v = 100
v = 6468;Treasure Chest
v = 100
v = 6470;Deep Sea Diver
v = 100
v = 8135;Underwater Cave
v = 12
v = 8136;Sunken Ship
v = 12
;SHELTERS
[cCompatibleTerrain]
v = 16
v = -5
v = 2
v = -5
v = 3
v = -5
v = 4
v = -5
v = 5
v = -5
v = 6
v = -5
v = 8
v = -50
v = 10
v = 100
v = 11
v = -5
v = 13
v = -5
[m/Animations]
; default idle animation
idle = subswim
; surface animations
surfidle = surfrest
surfswim = surfswim
surface_rest = surfrest
surface_tentacle = surftent
2_surface_rest = 2surfres
; surface animation shadows
shadowsurfswim = ssurfswi
shadowsurfrest = ssurfres
shadowsurftent = ssurften
shadow2surfres = s2surfre
; surface animations underwater half
undersurfswim = usurfswi
undersurfrest = usurfres
undersurftent = usurften
under2surfres = u2surfre
; submerged animations
subidle = subrest
subswim = subswim
rise = rise
dive = dive
submerged_rest = subrest
submerged_1_camo = sub1camo
submerged_2_camo = sub2camo
submerged_eat = subeat
submerged_ink = subink
submerged_sick = subsick
submerged_tentacle = subtent
2_submerged_eat = 2subeat
2_submerged_rest = 2subrest
2_submerged_sick = 2subsick
; submerged animation shadows
shadowsubswim = ssubswim
shadowsubrest = ssubrest
shadowrise = srise
shadowdive = sdive
shadowsub1camo = ssub1cam
shadowsub2camo = ssub2cam
shadowsubeat = ssubeat
shadowsubink = ssubink
shadowsubsick = ssubsick
shadowsubtent = ssubtent
shadow2subeat = s2subeat
shadow2subrest = s2subres
shadow2subsick = s2subsic
;Box and dustball animations
box_idle = objects/aquabox2/idle/idle.ani
box_used = objects/aquabox2/used/used.ani
dustball = objects/dustcld/idle/idle.ani
waterball = objects/watcloud/idle/idle.ani
surfaceball = objects/surfcld/surfc/surfc.ani
shadowsurfc = objects/surfcld/ssurfc/ssurfc.ani
undersurfc = objects/surfcld/usurfc/usurfc.ani
submergedball = objects/subcld/subc/subc.ani
shadowsubc = objects/subcld/ssubc/ssubc.ani
[Sounds]
surftent = animals/gsquid/surftent.wav
surftent = 1500
dustball = animals/sealion/splash2.wav
dustball = 1500
waterball = animals/sealion/splash1.wav
waterball = 1500
surfaceball = animals/sealion/splash2.wav
surfaceball = 800
submergedball = animals/sealion/splash1.wav
submergedball = 800
placesound = sounds/splash2.wav
placesound = 1500
pickupsound = sounds/submrg3.wav
pickupsound = 1500
;Giant Squid male underwater behavior set probabilities
[m\BehaviorSet\bHappyU]
f = fPlaySetProb(bSubswim,50,bSubrise,5,bSubdive,15,bSubtosurf,5,bFloor,15,bSubrest,10)
[m\BehaviorSet\bFriendlyU]
f = fPlaySetProb(bSubswim,50,bSubrise,5,bSubdive,15,bSubtosurf,5,bFloor,15,bSubrest,10)
[m\BehaviorSet\bCalmU]
f = fPlaySetProb(bSubswim,50,bSubrise,5,bSubdive,15,bSubtosurf,5,bFloor,15,bSubrest,10)
[m\BehaviorSet\bNeutralU]
f = fPlaySetProb(bSubswim,50,bSubrise,5,bSubdive,15,bSubtosurf,5,bFloor,15,bSubrest,5,bSubtent,5)
[m\BehaviorSet\bUneasyU]
f = fPlaySetProb(bSubswim,40,bSubrise,8,bSubdive,15,bSubtosurf,5,bFloor,15,bSubrest,5,bSubcamo1,3,bSubcamo2,2,bSubtent,5,bSubink,2)
[m\BehaviorSet\bTenseU]
f = fPlaySetProb(bSubswim,40,bSubrise,7,bSubdive,15,bSubtosurf,5,bFloor,15,bSubrest,5,bSubcamo1,3,bSubcamo2,2,bSubtent,5,bSubink,3)
[m\BehaviorSet\bAngryU]
f = fPlaySetProb(bSubswim,40,bSubrise,5,bSubdive,15,bSubtosurf,5,bFloor,15,bSubrest,5,bSubcamo1,3,bSubcamo2,2,bSubtent,5,bSubink,5)
;Giant Squid male underwater behavior sets
[m\BehaviorSet\bSubswim]
f = fWalk(level, 0)
[m\BehaviorSet\bSubrise]
f = fWalk(rise, 0)
[m\BehaviorSet\bSubdive]
f = fWalk(dive, 0)
[m\BehaviorSet\bSubtosurf]
f = fWalk(surface, 0)
[m\BehaviorSet\bFloor]
f = fWalk(floor, 0)
[m\BehaviorSet\bSubrest]
f = fPlayTime(submerged_rest,9)
[m\BehaviorSet\bSubcamo1]
f = fPlay(submerged_1_camo)
[m\BehaviorSet\bSubcamo2]
f = fPlay(submerged_2_camo)
[m\BehaviorSet\bSubink]
f = fPlay(submerged_ink)
[m\BehaviorSet\bSubtent]
f = fPlay(submerged_tentacle)
;Giant Squid male surface behavior set probabilities
[m\BehaviorSet\bHappyS]
f = fPlaySetProb(bSurfswim,10,bSurfloor,35,bSurfdive,40,bSurfrest,5,bSurftent,10)
[m\BehaviorSet\bFriendlyS]
f = fPlaySetProb(bSurfswim,10,bSurfloor,35,bSurfdive,40,bSurfrest,5,bSurftent,10)
[m\BehaviorSet\bCalmS]
f = fPlaySetProb(bSurfswim,10,bSurfloor,35,bSurfdive,40,bSurfrest,5,bSurftent,10)
[m\BehaviorSet\bNeutralS]
f = fPlaySetProb(bSurfswim,10,bSurfloor,35,bSurfdive,40,bSurfrest,5,bSurftent,10)
[m\BehaviorSet\bUneasyS]
f = fPlaySetProb(bSurfswim,10,bSurfloor,35,bSurfdive,40,bSurfrest,5,bSurftent,10)
[m\BehaviorSet\bTenseS]
f = fPlaySetProb(bSurfswim,10,bSurfloor,35,bSurfdive,40,bSurfrest,5,bSurftent,10)
[m\BehaviorSet\bAngryS]
f = fPlaySetProb(bSurfswim,10,bSurfloor,35,bSurfdive,40,bSurfrest,5,bSurftent,10)
;Giant Squid male surface Behavior Sets
[m\BehaviorSet\bSurfswim]
f = fMove(level, 0, surfswim, 25)
[m\BehaviorSet\bSurfdive]
f = fWalk(dive, 0)
[m\BehaviorSet\bSurfloor]
f = fWalk(floor, 0)
[m\BehaviorSet\bSurfrest]
f = fPlayTime(surface_rest,9)
[m\BehaviorSet\bSurftent]
f = fPlayWithSound(surface_tentacle,surftent)
[m\BehaviorSet\bFindKeeperFood]
f = fWalk(keeperfood,0)
f = fPlaySet(bEat)
[m\BehaviorSet\bEat]
f = fFaceTowardFood()
f = fPlay(2_submerged_eat)
f = fPlayTime(submerged_eat,9)
f = fPlay(subidle)
[m\BehaviorSet\bSick]
f = fPlaySetTank(bSurfsick, bSubsick)
[m\BehaviorSet\bSurfsick]
f = fPlay(2_surface_rest)
f = fPlay(surface_rest)
f = fPlayTime(surface_rest,9)
f = fPlayReverse(2_surface_rest)
[m\BehaviorSet\bSubsick]
f = fPlay(2_submerged_sick)
f = fPlay(submerged_sick)
f = fPlayTime(submerged_sick,9)
f = fPlayReverse(2_submerged_sick)
[m\BehaviorSet\bDie]
f = fPlaySetTank(bSurfdie, bSubdie)
[m\BehaviorSet\bSurfdie]
f = fPlay(2_surface_rest)
f = fPlay(surface_rest)
f = fPlayTime(surface_rest,5)
f = fDie()
[m\BehaviorSet\bSubdie]
f = fPlay(2_submerged_sick)
f = fPlay(submerged_sick)
f = fPlayTime(submerged_sick,5)
f = fDie()
[m\BehaviorSet\bSleep]
f = fPlaySetTank(bSurfsleep, bSubsleep)
[m\BehaviorSet\bSurfsleep]
f = fPlay(2_surface_rest)
f = fPlay(surface_rest)
f = fPlayTime(surface_rest,9)
f = fPlayReverse(2_surface_rest)
[m\BehaviorSet\bSubsleep]
f = fPlay(2_submerged_rest)
f = fPlay(submerged_rest)
f = fPlayTime(submerged_rest,9)
f = fPlayReverse(2_submerged_rest)
[m\BehaviorSet\bWalk]
f = fWalk(0,0)
[m\BehaviorSet\bRun]
f = fRun(0,0)
[m\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
[m\BehaviorSet\bChasePrey]
f = fRun(prey,0)
[m\BehaviorSet\bCaughtGuest]
; the time of animation below should match bCaughtBy in guest
f = fPlaySetTerrain(bcgland,bcgwater,bcgsurf,bcgsub)
[m\BehaviorSet\bcgland]
f = fPlayTime(stand,4)
[m\BehaviorSet\bcgwater]
f = fPlayTime(surfidle,4)
[m\BehaviorSet\bcgsurf]
f = fPlayTime(surfidle,4)
[m\BehaviorSet\bcgsub]
f = fPlayTime(subidle,4)
[m\BehaviorSet\bRunFromPredator]
f = fRun(0,0)
[m\BehaviorSet\bCaughtPrey]
f = fDustBall()
f = fPlaySetTank(bAcsurf,bAcsub)
[m\BehaviorSet\bAcsurf]
f = fPlay(surfidle)
[m\BehaviorSet\bAcsub]
f = fPlay(subidle)
[AmbientAnimsUnderwater]
a = 81
a = 100
b = bHappyU
a = 61
a = 80
b = bFriendlyU
a = 21
a = 60
b = bCalmU
a = -20
a = 20
b = bNeutralU
a = -60
a = -21
b = bUneasyU
a = -80
a = -61
b = bTenseU
a = -100
a = -81
b = bAngryU
[AmbientAnimsSurface]
a = 81
a = 100
b = bHappyS
a = 61
a = 80
b = bFriendlyS
a = 21
a = 60
b = bCalmS
a = -20
a = 20
b = bNeutralS
a = -60
a = -21
b = bUneasyS
a = -80
a = -61
b = bTenseS
a = -100
a = -81
b = bAngryS