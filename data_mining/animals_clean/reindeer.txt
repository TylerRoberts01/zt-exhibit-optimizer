[Global]
Class = animals
Type = reindeer
Subtype = 
DefaultSubtype = m
[AnimPath]
m = animals/reindeer/m
f = animals/reindeer/m
y = animals/reindeer/y
[m/Icon]
Icon = animals/reindeer/icmreind/icmreind
[Characteristics/Integers]
[Member]
animals
[f/Characteristics/Strings]
[m/Characteristics/Strings]
cKeeperFoodType = herbivore
cGeneralInfoTextName = REINDEER_GENERAL
cPlaqueImageName = animals/reindeer/plmreind/plmreind
cListImageName = animals/reindeer/lsmreind/lsmreind
cPrefIcon = objects/xmastree/SE/SE; Christmas tree
[f/Characteristics/Integers]
[m/Characteristics/Integers]
cKeeperFoodType = 0
cPrefIconID = 7077; Xmas tree
cNameID = 5300; Reindeer
cHelpID = 5300
cFamily = 5205; Even-toed Ungulate
cGenus = 5129; Deer
cHabitat = 9408; Tundra
cLocation = 9608; Arctic
cPurchaseCost = 400
cInitialHappiness  = 50
cSlowRate = 18
cMediumRate = 42
cFastRate = 76
cFootprintX = 1
cFootprintY = 2
cCaptivity = 5;//Angry Effects//
cNoFoodChange  = -50
cSickChange = -8	
cOtherAnimalSickChange = -5		
cCrowdHappinessChange = -5
cOtherAnimalAngryChange = 0 
cNumberMinChange = -10
cNumberMaxChange = -5
cAllCrowdedChange = -5
cAngryHabitatChange = -10
cVeryAngryHabitatChange = -30
cDeathChange = -5 
cNoMateChange = 0
cNotEnoughKeepersChange = -10
cAngryTreeChange = -5
cBabyBornChange = 30;//Happy Effects//
cHappyHabitatChange = 20
cMaxHits  = 160
cMinHits  = 0
cPctHits  = 20
cHungerThreshold  = 50
cHungerIncrement  = 5
cNeededFood = 80
cKeeperFoodUnitsEaten = 10
cHungryHealthChange = 10
cSickChance = 3
cEnergyThreshold = 100 
cEnergyIncrement  = 12	
cMaxEnergy = 100
cCrowd = 30
cKeeperFrequency = 2
cSocial = 1
cHabitatSize = 100
cNumberAnimalsMin = 6
cNumberAnimalsMax = 15
cAnimalDensity = 30
cPctHabitat = 10
cHabitatPreference = 80
cDirtyHabitatRating = 40
cDirtyIncrement = 8
cDirtyThreshold = 100
cReproductionChance = 2 
cReproductionInterval = 1800
cBabyToAdult = 720
cHappyReproduceThreshold = 97
cOffspring = 1
cMatingType = 0
cIsJumper = 1
cIsClimber = 0
cBashStrength = 0
cFlies = 0
cSwims = 0
cEscapedChange = 30
cSickTime = 4
cMimic = 70
cMimicHappyDiff = 50
cOtherFood = 0
cFoodTypes = 0
cTreePref = 3
cRockPref = 2
cSpacePref = 60 
cElevationPref = 0
cTimeDeath = 8640
cDeathChance = 10 
cEatVegetationChance = 20
cDrinkWaterChance = 10
cChaseAnimalChance = 12
cClimbsCliffs = 0
cBuildingUseChance = 10
cAttractiveness = 45
cNeedShelter = 1
[y/Characteristics/Integers]
cChaseAnimalChance = 22
cIsJumper = 0
cAttractiveness = 55
[m/Characteristics/Mixed]
[cCompatibleAnimals]
v = 5200;Primitive ungulates		
v = -10
v = 5201;Racoon			
v = -10
v = 5202;Bears			
v = -25
v = 5203;Primates			
v = -15
v = 5204;Odd-toed ungulates		
v = -5
v = 5205;Even-toed ungulates			
v = -5
v = 5206;Cat			
v = -25
v = 5207;Canine			
v = -25
v = 5208;Hyena			
v = -25
v = 5210;Marsupial		
v = -10
v = 5211;Bird			
v = -5
v = 5212;Seal			
v = -5
v = 5213;Crocodile		
v = -20
v = 5214;Edentate		    	
v = -5
v = 5215;Pig			
v = -5
v = 5216;Dinosaur			
v = -20
v = 5217;Monster			
v = -20
v = 5300;Reindeer			
v = 5
[cSuitableObjects]
v = 9410;Non-habitat
v = -10
v = 9405;Tropical rainforest
v = -25
v = 9400;Savannah
v = -25
v = 9401;Grassland
v = -15
v = 9402;Deciduos forest
v = -10
v = 9403;Coniferous forest
v = -5
v = 9407;Highlands
v = -5
v = 9408;Tundra
v = 3
v = 9409;Desert
v = -25
v = 9413;Aquatic
v = -10
v = 9414;Bad-habitat
v = -100
;FOLIAGE AND ROCKS
v = 9220;lrocka1
v = 4
v = 9221;lrocka2
v = 4
v = 9211;bwallsnw
v = 2
v = 7023;wspruce
v = 10
v = 7024;westceda
v = 7
v = 7068;wrcedar
v = 7
v = 7077;xmastree
v = 10
v = 6062;Poo
v = -2
;SHELTERS
v = 8113;Stable1
v = 15
v = 8114;Stable2
v = 20
v = 8115;Stable3
v = 25
[cCompatibleTerrain]
v = 16
v = -5
v = 1
v = -50
v = 2
v = -25
v = 3
v = -5
v = 6
v = 10
v = 8
v = 75
v = 0
v = 15
v = 10
v = -20
[m/Animations]
idle = stndidle
eat = eat
fly_shoo = flyshoo
head_down = headdwn
head_ground = headgrnd
raise_head = headrais
head_shoulder = headshou
horns = horns
jerk_head = jerk
look_left = lookl
look_right = lookr
rub = rub
scuff_feet = scuff
shake_head = shake
stamp_feet = stamp
stand = stndidle
walk = walk
trot = trot
run = run
jump_high = highjump
star_start = starjump
star_land = starland
star_run = starrun
;From lying down
lie_down = liedown
lie_idle = lieidle
lie_roll = lieroll
lie_side = lieside
side_idle = sideidle
sleep = sleep
; Box animations
box_idle = objects/box/idle/idle.ani
box_used = objects/box/used/used.ani
[y/Animations]
idle = stndidle
eat = eat
fly_shoo = flyshoo
head_down = headdwn
head_ground = headgrnd
raise_head = headrais
head_shoulder = headshou
horns = horns
jerk_head = jerk
look_left = lookl
look_right = lookr
rub = rub
scuff_feet = scuff
shake_head = shake
stamp_feet = stamp
stand = stndidle
walk = walk
trot = trot
run = run
jump_high = run
star_start = starjump
star_land = starland
star_run = starrun
;From lying down
lie_down = liedown
lie_idle = lieidle
lie_roll = lieroll
lie_side = lieside
side_idle = sideidle
sleep = sleep
; Box animations
box_idle = objects/box/idle/idle.ani
box_used = objects/box/used/used.ani
[Sounds]
maa = rein1
maa = 1100
maa2 = rein2
maa2 = 1100
maa3 = rein3
maa3 = 1100
placesound = rein1
placesound = 1100
pickupsound = rein2
pickupsound = 1100
[m\BehaviorSet\bHappy]
f = fPlaySetProb(bIdle1,20,bHappy1,50,bHappy2,25,bFly,5)
;f = fPlaySetProb(bSoundtest1,100)
[m\BehaviorSet\bFriendly]
f = fPlaySetProb(bHappy3,60,bIdle2,40)
[m\BehaviorSet\bCalm]
f = fPlaySetProb(bCalm1,50,bIdle1,30,bIdle2,20)
[m\BehaviorSet\bNeutral]
f = fPlaySetProb(bIdle1,50,bIdle2,50)
[m\BehaviorSet\bUneasy]
f = fPlaySetProb(bUneasy1,60,bIdle2,40)
[m\BehaviorSet\bTense]
f = fPlaySetProb(bTense1,50,bAngry3,20,bIdle3,30)
[m\BehaviorSet\bAngry]
f = fPlaySetProb(bAngry1,50,bAngry2,30,bIdle3,20)
[m\BehaviorSet\bEscaped]
f = fPlaySetProb(bEscaped1,80,bNoise1,20)
[m\BehaviorSet\bBabyActions]
f = fPlaySetProb(Baby1,40,bBaby2,40,bIdle1,20)
[m\BehaviorSet\bPlayToys]
;f = fPlaySetProb(bToy3,20,bSpray1,20,bLog1,20,bToy1,20,bToy2,20)
[m\BehaviorSet\bPlayWithToyHappy]
;f = fPlaySetProb(bPlayToys,50,bIdle1,20,bHappy2,20,bNoise1,10)
[m\BehaviorSet\bPlayWithToyFriendly]
;f = fPlaySetProb(bPlayToy,50,bIdle2,30,bNoise2,20)
[m\BehaviorSet\bSoundtest1]
;f = fPlayWithSound(jerk_head,maa)
;f = fPlayWithSound(shake_head,maa)
;f = fPlayWithSound(raise_head,maa)
[m\BehaviorSet\bFly]
f = fPlay(stand)
f = fMove(0,0,star_run,76)
f = fPlay(stand)
[m\BehaviorSet\bOthertest1]
;f = fMove(0,0,walk,18)
;f = fMove(0,0,trot,42)
;f = fMove(0,0,run,76)
[m\BehaviorSet\bHappy1]
f = fPlay(stand)
f = fPlay(head_shoulder)
f = fPlay(stand)
f = fWalk(0,0)
f = fPlayWithSound(shake_head,maa)
f = fPlay(stand)
f = fMove(0,0,trot,42)
f = fPlay(stand)
[m\BehaviorSet\bHappy2]
f = fPlay(stand)
f = fPlay(fly_shoo)
f = fMove(0,0,trot,42)
f = fPlay(stand)
f = fPlayWithSound(shake_head,maa)
f = fPlayPingPong(look_left)
f = fPlay(stand)
[m\BehaviorSet\bHappy3]
f = fPlay(stand)
f = fMove(0,0,run,76)
f = fPlay(stand)
f = fPlayWithSound(shake_head,maa3)
f = fPlayPingPong(look_right)
f = fPlayTime(stand,6)
f = fPlay(stand)
[m\BehaviorSet\bCalm1]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,8)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[m\BehaviorSet\bUneasy1]
f = fPlay(scuff_feet)
f = fPlay(stand)
f = fMove(0,0,trot,42)
f = fPlayTime(stand,3)
f = fPlayWithSound(jerk_head,maa3)
f = fPlay(stand)
[m\BehaviorSet\bTense1]
f = fPlay(stand)
f = fWalk(0,0)
f = fPlay(stand)
f = fPlayWithSound(jerk_head,maa2)
f = fPlayPingPong(horns)
f = fPlay(stand)
[m\BehaviorSet\bAngry1]
f = fPlay(stand)
f = fPlayWithSound(jerk_head,maa2)
f = fPlayTime(stand,7)
f = fMove(0,0,run,76)
f = fPlay(stamp_feet)
f = fPlay(stand)
f = fPlayWithSound(shake_head,maa)
f = fPlay(stand)
[m\BehaviorSet\bAngry2]
f = fMove(0,0,run,76)
f = fPlay(stand)
f = fPlay(stamp_feet)
f = fPlayTime(stand,6)
f = fPlayWithSound(scuff_feet,maa)
f = fPlay(stand)
f = fMove(0,0,trot,42)
f = fPlayWithSound(jerk_head,maa2)
f = fPlay(stand)
[m\BehaviorSet\bAngry3]
f = fPlay(stand)
f = fPlay(scuff_feet)
f = fPlayTime(stand,6)
f = fPlayWithSound(shake_head,maa3)
f = fMove(0,0,trot,42)
f = fPlay(stand)
[m\BehaviorSet\bNoise1]
f = fPlay(stand)
f = fPlayWithSound(jerk_head,maa2)
f = fPlay(stand)
[m\BehaviorSet\bNoise2]
f = fPlay(stand)
f = fPlayWithSound(shake_head,maa)
f = fPlay(stand)
[m\BehaviorSet\bIdle1]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,24)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[m\BehaviorSet\bIdle2]
f = fPlay(stand)
f = fPlay(shake_head)
f = fPlayPingPong(look_right)
f = fPlay(stand)
[m\BehaviorSet\bIdle3]
f = fPlay(stand)
f = fPlay(head_shoulder)
f = fPlayTime(stand,12)
f = fPlay(jerk_head)
f = fPlay(stand)
[m\BehaviorSet\bSleep]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlay(lie_idle)
f = fPlay(head_down)
f = fPlayTime(sleep,25)
f = fPlayReverse(head_down)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[m\BehaviorSet\bDie]
f = fPlay(lie_down)
f = fPlay(head_down)
f = fPlayTime(sleep,5)
f = fPlayTime(sleep,5)
f = fDie()
[m\BehaviorSet\bEat]
f = fFaceTowardFood()
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,4)
f = fPlayReverse(head_ground)
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,8)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[m\BehaviorSet\bSick]
f = fPlay(lie_down)
f = fPlay(lie_idle)
f = fPlay(lie_side)
f = fPlayTime(side_idle,18)
f = fPlayReverse(lie_side)
f = fPlay(lie_idle)
f = fPlayReverse(lie_down)
[m\BehaviorSet\bFindKeeperFood]
f = fWalk(keeperfood,0)
[m\BehaviorSet\bFindOtherFood]
f = fWalk(otherfood,0)
[m\BehaviorSet\bEscaped1]
f = fPlay(stand)
f = fMove(0,0,run,76)
f = fPlayWithSound(shake_head,maa)
f = fPlay(stand)
[m\BehaviorSet\bChasePrey]
f = fMove(prey,0,"run",76)
[m\BehaviorSet\bRunFromPredator]
f = fMove(prey,0,run,76)
[m\BehaviorSet\bCaught]
f = fFollow(keeper)
[m\BehaviorSet\bBaby1]
f = fPlay(stand)
f = fWalk(mother,0)
[m\BehaviorSet\bBaby2]
f = fPlay(stand)
f = fWalk(mother,0)
[m\BehaviorSet\bBored1]
f = fPlayTime(stand,10)
[m\BehaviorSet\bBored2]
f = fPlayTime(stand,10)
[m\BehaviorSet\bAngryKeeper]
f = fPlay(stand)
[m\BehaviorSet\bWalk]
f = fWalk(0,0)
[m\BehaviorSet\bRun]
f = fMove(0,0,run,76)
[m\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
[AmbientAnims]
;a = -100
;a = 100
;b = bSoundtest1
;a = -100
;a = 100
;b = bFly
a = 81
a = 100
b = bHappy
a = 61
a = 80
b = bFriendly
a = 21
a = 60
b = bCalm
a = -20
a = 20
b = bNeutral
a = -60
a = -21
b = bUneasy
a = -80
a = -61
b = bTense
a = -100
a = -81
b = bAngry
