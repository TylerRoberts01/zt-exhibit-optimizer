[Global]
Class = animals
Type = baracuda
Subtype = 
DefaultSubtype = m
[AnimPath]
m = animals/baracuda/m
f = animals/baracuda/m
y = animals/baracuda/y
[m/Icon]
Icon = animals/baracuda/icbaracu/icbaracu
[Characteristics/Integers]
; shadows for this unit
cUsesRealShadows = 1
cHasShadowImages = 1
cHasUnderwaterSection = 1
cExpansionID=2
[Member]
animals
aqua
[f/Characteristics/Strings]
[m/Characteristics/Strings]
cKeeperFoodType = carnivore
cGeneralInfoTextName = BARACUDA_GENERAL
cPlaqueImageName = animals/baracuda/plbaracu/plbaracu
cListImageName = animals/baracuda/lsbaracu/lsbaracu
cPrefIcon = objects/sgrass/SE/SE;Sea Grass
[f/Characteristics/Integers]
[m/Characteristics/Integers]
cKeeperFoodType = 10
cPrefIconID = 7417;Sea Grass
cNameID = 5423; Great Baracuda
cHelpID = 5423
cFamily = 5229; Perciformes
cGenus = 5182; Baracuda
cHabitat = 9413; Aquatic
cLocation = 9625; Many
cPurchaseCost = 650
cInitialHappiness  = 70
cSlowRate = 66
cMediumRate = 75
cFastRate = 100
cFootprintX = 2
cFootprintY = 4
cIconZoom = 0
cBoxedIconZoom = 0
cCaptivity = 5;//ANGRY EFFECTS//
cNoFoodChange  = -50
cSickChange = -12
cOtherAnimalSickChange = -10	
cCrowdHappinessChange = -5
cOtherAnimalAngryChange = 0
cNumberMinChange = -5
cNumberMaxChange = -5
cAllCrowdedChange = -5
cAngryHabitatChange = -5
cVeryAngryHabitatChange = -30
cDeathChange = -5
cAngryTreeChange = -5
cNotEnoughKeepersChange = -20
cBabyBornChange = 30;//HAPPY EFFECTS//
cHappyHabitatChange = 20
cMaxHits  = 400
cMinHits  = 0
cPctHits  = 20
cHungerThreshold  = 50
cHungerIncrement  = 11
cNeededFood = 100
cKeeperFoodUnitsEaten = 25
cHungryHealthChange = 10
cSickChance = 5
cEnergyThreshold = 100
cEnergyIncrement  = 5
cMaxEnergy = 100
cCrowd = 100
cKeeperFrequency = 2
cSocial = 1
cHabitatSize = 100
cNumberAnimalsMin = 4
cNumberAnimalsMax = 12
cAnimalDensity = 5
cReproductionChance = 2
cReproductionInterval = 3240
cBabyToAdult = 1440
cHappyReproduceThreshold = 96
cOffspring = 1
cMatingType = 1
cNoMateChange = -10
cIsJumper = 0
cIsClimber = 0
cBashStrength = 0
cFlies = 0
cSwims = 0
cAvoidEdges = 1; Avoid tank walls
cUnderwater = 1; Underwater characteristics
cOnlyUnderwater = 1
cDepth = 1
cPctHabitat = 10
cHabitatPreference = 70
cEscapedChange = 50
cSickTime = 4
cMimic = 10
cMimicHappyDiff = 50
cOtherFood = 0
cFoodTypes = 0
cTreePref = 25
cRockPref = 10
cSpacePref = 80 
cElevationPref = 0
cDepthMin = 5
cDepthMax = 29
cDepthChange = -10
cSalinityChange = -10
cSalinityHealthChange = -20
cDirtyHabitatRating = 15
cDirtyIncrement = 10
cDirtyThreshold = 100
cPooWaterImpact = 5 
cMurkyWaterChange = -5
cMurkyWaterHealthChange = -5
cMurkyWaterThreshold = 60
cVeryMurkyWaterChange = -10
cVeryMurkyWaterHealthChange = -10
cVeryMurkyWaterThreshold = 20
cExtremelyMurkyWaterChange = -15
cExtremelyMurkyWaterHealthChange = -15
cExtremelyMurkyWaterThreshold= 1
cTimeDeath = 12960
cDeathChance = 10 
cEatVegetationChance = 0
cDrinkWaterChance = 0
cChaseAnimalChance = 50
cClimbsCliffs = 0
cBuildingUseChance = 0
cPrey = 9503;Man
cPrey = 5412;Moray Eel
cPrey = 5422;Bluefin Tuna
cPrey = 5039;Emporer Penguin
cPrey = 5415;Octopus
cPrey = 5403;Arctic Lion's Mane Jellyfish
cAttractiveness = 40
cNeedShelter = 0
cKeeper = 9552;Keeper is marine specialist
cKeepMoving = 1; perpetual movement
cStaySick = 1; stay in bSick until healed, then play bDoneSick
[y/Characteristics/Integers]
cIsJumper = 0
cChaseAnimalChance = 70
cAttractiveness = 45
[m/Characteristics/Mixed]
[cCompatibleAnimals]
v = 5200;Primitive ungulates		
v = -20
v = 5201;Racoon			
v = -10
v = 5202;Bears			
v = -20
v = 5203;Primates			
v = -5
v = 5204;Odd-toed ungulates		
v = -10
v = 5205;Even-toed ungulates			
v = -10
v = 5206;Cat			
v = -20
v = 5207;Canine			
v = -20
v = 5208;Hyena			
v = -20
v = 5210;Marsupial		
v = -5
v = 5211;Bird			
v = -5
v = 5212;Seal			
v = -15
v = 5213;Crocodile		
v = -20
v = 5214;Edentate		    	
v = -5
v = 5215;Pig			
v = -5
v = 5135;Tyrannosaurs
v = -10
v = 5136;Smilodons
v = -10
v = 5137;Ankylosaurs
v = -10
v = 5138;Gallimimuses
v = -10
v = 5139;Iguanodons
v = -10
v = 5140;Lambeosaurs
v = -10
v = 5141;Spinosaurs
v = -25
v = 5142;Styracosaurs
v = -10
v = 5143;Velociraptors
v = -10
v = 5144;Allosaurs
v = -10
v = 5145;Camptosaurs
v = -10
v = 5146;Caudipteryxes
v = -10
v = 5147;Kentrosaurs
v = -10
v = 5148;Plesiosaurs
v = -10
v = 5149;Stegosaurs
v = -10
v = 5150;Apatosaurs
v = -10
v = 5151;Coelophysises
v = -10
v = 5152;Herrerasaurs
v = -10
v = 5153;Plateosaurs
v = -10
v = 5154;Wooly Mammoths
v = -10
v = 5155;Wooly Rhinos
v = -10
v = 5156;Meiolanias
v = -10
v = 5157;Triceratopses
v = -10
v = 5158;Deinosuchus
v = -25
v = 5085;T-Rex			
v = -20
v = 5159;Orcas
v = -10
v = 5160;Great White Sharks
v = -20
v = 5161;Bottlenose Dolphins
v = -10
v = 5162;Jellyfishes
v = -10
v = 5163;Northern Elephant Seals
v = -10
v = 5164;Humpback Whales
v = -10
v = 5165;Sperm Whales
v = -10
v = 5166;Narwhals
v = -10
v = 5167;Harbor Porpoises
v = -10
v = 5168;Hammerhead Sharks
v = -10
v = 5169;Tiger Sharks
v = -10
v = 5170;Mako Sharks
v = -20
v = 5171;Moray Eels
v = -10
v = 5172;Belugas
v = -10
v = 5173;Sea Otters
v = -10
v = 5174;Octopuses
v = -10
v = 5175;Manatees
v = -10
v = 5176;Walruses
v = -10
v = 5177;Manta Rays
v = -20
v = 5178;Sea Turtles
v = -10
v = 5179;Swordfishes
v = -10
v = 5180;Giant Squids
v = -20
v = 5181;Bluefin Tuna
v = -10
v = 5182;Baracudas
v = -10
v = 5183;Mermaids
v = 10
v = 5185;Whale Sharks
v = -10
v = 5187;Bowhead Whales
v = -10
v = 5423;Baracuda			
v = 10
[cSuitableObjects]
v = 9410;Non-habitat
v = -10
v = 9405;Tropical rainforest
v = -25
v = 9400;Savannah
v = -25
v = 9401;Grassland
v = -25
v = 9402;Deciduous forest
v = -25
v = 9403;Coniferous forest
v = -25
v = 9407;Highlands
v = -25
v = 9408;Tundra
v = -10
v = 9409;Desert
v = -25
v = 9413;Aquatic
v = 0
v = 9414;Bad-habitat
v = -100
;FOLIAGE AND ROCKS
v = 7061;Water Reed
v = 0
v = 7062;Water Lily
v = 0
v = 7083;Bald Cypress
v = 0
v = 7088;Horse Tail
v = 0
v = 7400;Sea Anemone
v = -2
v = 7401;Barnacles
v = 0
v = 7402;Beach Grass
v = 0
v = 7403;Brittle Sea Star
v = -2
v = 7404;Clambed
v = 0
v = 7405;Orange Cup Coral
v = -2
v = 7406;Divercate Tree Coral
v = 6
v = 7407;Feather Duster Worm
v = 6
v = 7408;Fire Cup Coral
v = 6
v = 7409;Kelp
v = 0
v = 7410;Sea Lettuce
v = 0
v = 7411;Red Gorgonian
v = 6
v = 7412;Sargassum
v = 6
v = 7413;Sand Dollar
v = 6
v = 7414;Sea Cucumber
v = 6
v = 7415;Sea Star
v = 0
v = 7416;Sea Weed
v = 0
v = 7417;Sea Grass
v = 8
v = 7418;Sea Sponge
v = -2
v = 7419;Stovepipe Sponge
v = 6
v = 7420;Tubeworm
v = -2
v = 7421;Purple Sea Urchin
v = -2
v = 9234;Aquatic rock
v = 7
v = 9235;Small Oceanfloor Rock
v = 6
v = 9236;Medium Coral Formation
v = 6
v = 9237;Large Oceanfloor Rock
v = 6
v = 9238;Medium Oceanfloor Rock
v = 6
v = 9239;Large Coral Formation
v = 6
v = 9240;Iceberg
v = -2
v = 9241;Isle Rock
v = 6
v = 6062;Poo
v = -5
v = 6466;Fake Clam
v = 100
v = 6468;Treasure Chest
v = 100
v = 6470;Deep Sea Diver
v = 100
;SHELTERS
[cCompatibleTerrain]
v = 16
v = -5
v = 2
v = -5
v = 3
v = -20
v = 4
v = -5
v = 5
v = -5
v = 6
v = -5
v = 8
v = -50
v = 10
v = 100
v = 11
v = -5
v = 13
v = -5
[m/Animations]
; default idle animation
idle = subswim
walk = subswim
shadowidle = ssubswim
; required animations
subswim = subswim
subidle = subswim
shadowsubswim = ssubswim
dive = dive
rise = rise
shadowdive = sdive
shadowrise = srise
; surface animations
surface_2_sick = 2surfsic
surface_sick = surfsick
surfswim = surfswim
surfidle = surfswim
surface_thrash = surftrsh
surface_breach = surfbrsh
 
; surface animation shadows
shadow2surfsic = s2surfsi
shadowsurfsick = ssurfsic
shadowsurfswim = ssurfswi
shadowsurftrsh = ssurftrs
shadowsurfbrsh = ssurfbrs
 
; surface underwater half animations
under2surfsic = u2surfsi
undersurfsick = usurfsic
undersurfswim = usurfswi
undersurftrsh = usurftrs
undersurfbrsh = usurfbrs
 
; submerged animations
submerged_2_sick = 2subsick
submerged_sick = subsick
submerged_eat = subeat
; submerged animations shadows
shadow2subsick = s2subsic
shadowsubsick = ssubsick
shadowsubeat = ssubeat
; Box and dustball animations
box_idle = objects/aquabox/idle/idle.ani
box_used = objects/aquabox/used/used.ani
dustball = objects/dustcld/idle/idle.ani
waterball = objects/watcloud/idle/idle.ani
surfaceball = objects/surfcld/surfc/surfc.ani
shadowsurfc = objects/surfcld/ssurfc/ssurfc.ani
undersurfc = objects/surfcld/usurfc/usurfc.ani
submergedball = objects/subcld/subc/subc.ani
shadowsubc = objects/subcld/ssubc/ssubc.ani
[Sounds]
2subsick = animals/baracuda/2subsick.wav
2subsick = 800
2surfsic = animals/baracuda/2surfsic.wav
2surfsic = 800
subdive = animals/baracuda/subdive.wav
subdive = 800
subeat = animals/baracuda/subeat.wav
subeat = 800
subrise = animals/baracuda/subrise.wav
subrise = 800
subsick = animals/baracuda/subsick.wav
subsick = 800
subswim = animals/baracuda/subswim.wav
subswim = 800
subtrsh = animals/baracuda/subtrsh.wav
subtrsh = 800
surftrsh = animals/baracuda/surftrsh.wav
surftrsh = 800
dustball = animals/baracuda/surftrsh.wav
dustball = 800
waterball = animals/baracuda/surftrsh.wav
waterball = 800
surfaceball = animals/baracuda/surftrsh.wav
surfaceball = 800
submergedball = animals/baracuda/subeat.wav
submergedball = 800
placesound = animals/baracuda/subdive.wav
placesound = 1000
pickupsound = animals/baracuda/subrise.wav
pickupsound = 1000
;Baracuda male underwater behavior set probabilities
[m\BehaviorSet\bHappyU]
f = fPlaySetProb(bSubswim,50,bRisesurf,15,bRise,20,bSubdive,10,bSubfloordive,5)
[m\BehaviorSet\bFriendlyU]
f = fPlaySetProb(bSubswim,50,bRisesurf,15,bRise,20,bSubdive,10,bSubfloordive,5)
[m\BehaviorSet\bCalmU]
f = fPlaySetProb(bSubswim,50,bRisesurf,15,bRise,20,bSubdive,10,bSubfloordive,5)
[m\BehaviorSet\bNeutralU]
f = fPlaySetProb(bSubswim,50,bRisesurf,10,bRise,15,bSubdive,15,bSubfloordive,10)
[m\BehaviorSet\bUneasyU]
f = fPlaySetProb(bSubswim,50,bRisesurf,10,bRise,5,bSubdive,15,bSubfloordive,20)
[m\BehaviorSet\bTenseU]
f = fPlaySetProb(bSubswim,50,bRisesurf,10,bRise,5,bSubdive,15,bSubfloordive,20)
[m\BehaviorSet\bAngryU]
f = fPlaySetProb(bSubswim,50,bRisesurf,10,bRise,5,bSubdive,15,bSubfloordive,20)
;Baracuda male surface behavior set probabilities
[m\BehaviorSet\bHappyS]
f = fPlaySetProb(bSurfswim,85,bSurfloor,5,bSurfdive,10)
[m\BehaviorSet\bFriendlyS]
f = fPlaySetProb(bSurfswim,80,bSurfdive,10,bSurfloor,10)
[m\BehaviorSet\bCalmS]
f = fPlaySetProb(bSurfswim,75,bSurfdive,20,bSurfloor,5)
[m\BehaviorSet\bNeutralS]
f = fPlaySetProb(bSurfswim,80,bSurfdive,10,bSurfloor,10)
[m\BehaviorSet\bUneasyS]
f = fPlaySetProb(bSurfloor,15,bSurfdive,20,bSurfswim,60,bSurfbrch,5)
[m\BehaviorSet\bTenseS]
f = fPlaySetProb(bSurfloor,20,bSurfdive,20,bSurfswim,50,bSurftrsh,5,bSurfbrch,5)
[m\BehaviorSet\bAngryS]
f = fPlaySetProb(bSurfloor,15,bSurfdive,15,bSurfswim,50,bSurftrsh,10,bSurfbrch,10)
;Baracuda male surface behavior sets
[m\BehaviorSet\bSurfswim]
f = fWalk(surface, 0)
[m\BehaviorSet\bSurfdive]
f = fWalk(dive, 0)
[m\BehaviorSet\bSurfloor]
f = fWalk(floor, 0)
[m\BehaviorSet\bSurftrsh]
f = fMoveWithSound(surface, 0, surface_thrash, 66, surftrsh)
[m\BehaviorSet\bSurbrch]
f = fMoveWithSound(surface, 0, surface_breach, 66, surfbrch)
;Baracuda male submerged behavior sets
[m\BehaviorSet\bSubswim]
f = fWalk(level, 0)
[m\BehaviorSet\bRisesurf]
f = fWalk(surface, 0)
[m\BehaviorSet\bRise]
f = fWalk(rise, 0)
[m\BehaviorSet\bSubdive]
f = fWalk(dive, 0)
[m\BehaviorSet\bSubfloordive]
f = fWalk(floor, 0)
[m\BehaviorSet\bSleep]
f = fPlaySetTank(bSurfsleep, bSubsleep)
[m\BehaviorSet\bSurfsleep]
f = fWalk(surface, 0)
[m\BehaviorSet\bSubsleep]
f = fWalk(level, 0)
[m\BehaviorSet\bFindKeeperFood]
f = fWalk(keeperfood,0)
[m\BehaviorSet\bEat]
f = fMoveWithSound(level, 1, submerged_eat, 66, subeat)
[m\BehaviorSet\bSick]
f = fPlaySetTank(bSurfsick, bSubsick)
[m\BehaviorSet\bSurfsick]
f = fHalt()
f = fPlayWithSound(surface_2_sick,2surfsic)
f = fSetBase(surface_sick)
f = fKeepControl()
[m\BehaviorSet\bSubsick]
f = fHalt()
f = fPlayWithSound(submerged_2_sick,2subsick)
f = fSetBase(submerged_sick)
f = fKeepControl()
[m\BehaviorSet\bDoneSick]
f = fPlaySetTank(bSurfDoneSick,bSubDoneSick)
[m\BehaviorSet\bSurfDoneSick]
f = fPlayReverse(surface_2_sick)
f = fWalk(0,0)
[m\BehaviorSet\bSubDoneSick]
f = fPlayReverse(submerged_2_sick)
f = fWalk(0,0)
[m\BehaviorSet\bDie]
f = fPlaySetTank(bSurfdie, bSubdie)
[m\BehaviorSet\bSurfdie]
f = fHalt()
f = fPlayWithSound(surface_2_sick,2surfsic)
f = fSetBase(surface_sick)
f = fDie()
[m\BehaviorSet\bSubdie]
f = fHalt()
f = fPlayWithSound(submerged_2_sick,2subsick)
f = fSetBase(submerged_sick)
f = fDie()
[m\BehaviorSet\bWalk]
f = fWalk(0,0)
[m\BehaviorSet\bRun]
f = fRun(0,0)
[m\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
[m\BehaviorSet\bChasePrey]
f = fRun(prey,0)
[m\BehaviorSet\bCaughtGuest]
; the time of animation below should match bCaughtBy in guest
f = fPlaySetTerrain(bcgland,bcgwater,bcgsurf,bcgsub)
[m\BehaviorSet\bcgland]
f = fPlayTime(stand,4)
[m\BehaviorSet\bcgwater]
f = fPlayTime(surfidle,4)
[m\BehaviorSet\bcgsurf]
f = fPlayTime(surfidle,4)
[m\BehaviorSet\bcgsub]
f = fPlayTime(subidle,4)
[m\BehaviorSet\bRunFromPredator]
f = fRun(0,0)
[m\BehaviorSet\bCaughtPrey]
f = fDustBall()
f = fPlaySetTank(bAcsurf,bAcsub)
[m\BehaviorSet\bAcsurf]
f = fPlay(surfidle)
[m\BehaviorSet\bAcsub]
f = fPlay(subidle)
[AmbientAnimsUnderwater]
a = 81
a = 100
b = bHappyU
a = 61
a = 80
b = bFriendlyU
a = 21
a = 60
b = bCalmU
a = -20
a = 20
b = bNeutralU
a = -60
a = -21
b = bUneasyU
a = -80
a = -61
b = bTenseU
a = -100
a = -81
b = bAngryU
[AmbientAnimsSurface]
a = 81
a = 100
b = bHappyS
a = 61
a = 80
b = bFriendlyS
a = 21
a = 60
b = bCalmS
a = -20
a = 20
b = bNeutralS
a = -60
a = -21
b = bUneasyS
a = -80
a = -61
b = bTenseS
a = -100
a = -81
b = bAngryS