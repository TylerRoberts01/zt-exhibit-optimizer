[Global]
Class = animals
Type = zebra
Subtype = 
DefaultSubtype = m
[AnimPath]
m = animals/zebra/m
f = animals/zebra/m
y = animals/zebra/y
[m/Icon]
Icon = animals/zebra/iczebra/iczebra
[Characteristics/Integers]
[Member]
animals
[f/Characteristics/Strings]
[m/Characteristics/Strings]
cKeeperFoodType = herbivore
cGeneralInfoTextName = ZEBRA_GENERAL
cFoodInfoTextName = ZEBRA_ANIMAL
cHabitatInfoTextName = ZEBRA_HABITAT
cBehaviorInfoTextName = ZEBRA_BEHAVIOR
cKeeperInfoTextName = ZEBRA_KEEPER
cPlaqueImageName = animals/zebra/plzebra/plzebra
cListImageName = animals/zebra/lsmzebra/lsmzebra
cPrefIcon = objects/savgrass/SE/SE;Tall savannah grass 
[f/Characteristics/Integers]
[m/Characteristics/Integers]
cKeeperFoodType = 0
cPrefIconID = 7060; Tall savannah grass
cNameID = 5004; Plains Zebra
cHelpID = 5004
cFamily = 5204; Odd-toed Ungulate
cGenus = 5104; Horse
cHabitat = 9400; Savannah
cLocation = 9600; Africa
cPurchaseCost = 800
cInitialHappiness  = 75
cSlowRate = 32
cMediumRate = 52
cFastRate = 72
cFootprintX = 1
cFootprintY = 2
cCaptivity = 5;//ANGRY EFFECTS//
cNoFoodChange  = -50
cSickChange = -20	
cOtherAnimalSickChange = -5		
cCrowdHappinessChange = -5
cOtherAnimalAngryChange = 0 
cNumberMinChange = -5
cNumberMaxChange = -5
cAllCrowdedChange = -5
cAngryHabitatChange = -5
cVeryAngryHabitatChange = -30
cDeathChange = -5 
cNoMateChange = 0
cNotEnoughKeepersChange = -10
cAngryTreeChange = -20
cBabyBornChange = 30;//HAPPY EFFECTS//
cHappyHabitatChange = 20
cMaxHits  = 120
cMinHits  = 0
cPctHits  = 20
cHungerThreshold  = 50
cHungerIncrement  = 3
cNeededFood = 60
cKeeperFoodUnitsEaten = 5
cHungryHealthChange = 10
cSickChance = 12
cEnergyThreshold = 100 
cEnergyIncrement  = 5	
cMaxEnergy = 100
cCrowd = 30
cKeeperFrequency = 2
cSocial = 1
cHabitatSize = 100
cNumberAnimalsMin = 3
cNumberAnimalsMax = 20
cAnimalDensity = 15
cPctHabitat = 10
cHabitatPreference = 50
cDirtyHabitatRating = 40
cDirtyIncrement = 4
cDirtyThreshold = 100
cReproductionChance = 2 
cReproductionInterval = 1600
cBabyToAdult = 1000
cHappyReproduceThreshold = 90
cOffspring = 1
cMatingType = 0
cIsJumper = 1
cIsClimber = 0
cBashStrength = 0
cFlies = 0
cSwims = 0
cEscapedChange = 30
cSickTime = 4
cMimic = 70
cMimicHappyDiff = 50
cOtherFood = 0
cFoodTypes = 0
cTreePref = 2
cRockPref = 2
cSpacePref = 80 
cElevationPref = 0
cTimeDeath = 4320
cDeathChance = 10 
cEatVegetationChance = 25
cDrinkWaterChance = 10
cChaseAnimalChance = 20
cClimbsCliffs = 0
cBuildingUseChance = 10
cAttractiveness = 5
cNeedShelter = 1
[y/Characteristics/Integers]
cChaseAnimalChance = 30
cIsJumper = 0
cAttractiveness = 15
[m/Characteristics/Mixed]
[cCompatibleAnimals]
v = 5200;Primitive ungulates		
v = 5
v = 5201;Racoon			
v = -5
v = 5202;Bears			
v = -20
v = 5203;Primates			
v = -5
v = 5204;Odd-toed ungulates		
v = 5
v = 5205;Even-toed ungulates			
v = -5
v = 5206;Cat			
v = -20
v = 5207;Canine			
v = -20
v = 5208;Hyena			
v = -20
v = 5210;Marsupial		
v = -5
v = 5211;Bird			
v = 5
v = 5212;Seal			
v = -5
v = 5213;Crocodile		
v = -20
v = 5214;Edentate		    	
v = -5
v = 5215;Pig			
v = 5
v = 5216;Dinosaur			
v = -20
v = 5217;Monster			
v = -20
v = 5004;Zebra			
v = -5
v = 5039;Penguin			
v = -10
v = 5005;Gazelle			
v = 10
v = 5031;Gemsbok			
v = 10
v = 5033;Giraffe			
v = 10
v = 5035;Hippo			
v = 10
v = 5028;Okapi			
v = 10
v = 5025;Wildebeest			
v = 10
v = 5030;African Buffalo			
v = 10
[cSuitableObjects]
v = 9400;Savannah
v = 3
v = 9410;Non-habitat
v = -15
v = 9408;Tundra
v = -25
v = 9402;Deciduos forest
v = -10
v = 9403;Coniferous forest
v = -15
v = 9404;Boreal forest
v = -15
v = 9405;Tropical rainforest
v = -15
v = 9407;Highlands
v = -10
v = 9409;Desert
v = -15
v = 9413;Aquatic
v = -10
v = 6062;Poo
v = -5
v = 9414;Bad-habitat
v = -100
;FOLIAGE AND ROCKS
v = 7061;water lilly
v = 10
v = 7062;water reed
v = 10
v = 7088;horsetail
v = 10
v = 7083;bald cypress
v = 10
v = 7048;bush1
v = -3
v = 7060;savgrass
v = 7
v = 7058;umbacac
v = 3
v = 7057;baobab
v = -3
v = 7063;grasstr
v = -8
v = 7030;redgum
v = -8
v = 7019;hquandon
v = -8
v = 7017;eucalyp
v = -8
v = 9200;lrock1
v = 4
v = 9205;srock1
v = 2
v = 9206;srock2
v = 2
v = 7076;khejri
v = -8
;SHELTERS
Leanto1
v = 8104
v = 20
Leanto2
v = 8105
v = 22
Leanto3
v = 8106
v = 26
v = 8107;Conhous1
v = 17
v = 8108;Conhous2
v = 20
v = 8109;Conhous3
v = 22
v = 8110;Wodhous1
v = 17
v = 8111;Wodhous2
v = 20
v = 8112;Wodhous3
v = 22
[cCompatibleTerrain]
v = 16
v = -5
v = 1
v = 95
v = 2
v = -5
v = 4
v = -5
v = 5
v = -5
v = 6
v = -5
v = 8
v = -50
v = 9
v = 5
v = 10
v = -20
v = 11
v = -5
v = 13
v = -5
[m/Animations]
idle = stand
stand = stand
lie_to_stand = standup
walk = walk
buck = buck
shoo_fly = flyshoo
head_ground = headgrnd
eat = eat
jerk_head = jerk
head_shoulder = headshou
look_left = lookl
look_right = lookr
rearup = rearup
scuff_feet = scuff
rub = rub
shake_head = shake
stamp_feet = stand
charge = charge
jump_high = highjump
lie_down = liedown
lie_idle = lieidle
lie_side = lieside
prance = prance
run = run
trot = trot
; Box animations
box_idle = objects/box/idle/idle.ani
box_used = objects/box/used/used.ani
[Sounds]
yip = zebra1
yip = 1300
snort = zebra2
snort = 1500
panic = zebra3
panic = 1100
placesound = zebra2
placesound = 1500
pickupsound = zebra1
pickupsound = 1300
[m\BehaviorSet\bHappy]
f = fPlaySetProb(bIdle1,20,bHappy1,50,bHappy2,30)
[m\BehaviorSet\bFriendly]
f = fPlaySetProb(bHappy3,60,bIdle2,40)
[m\BehaviorSet\bCalm]
f = fPlaySetProb(bCalm1,50,bIdle1,30,bIdle2,20)
[m\BehaviorSet\bNeutral]
f = fPlaySetProb(bIdle1,50,bIdle2,50)
[m\BehaviorSet\bUneasy]
f = fPlaySetProb(bUneasy1,60,bIdle2,40)
[m\BehaviorSet\bTense]
f = fPlaySetProb(bTense1,50,bAngry3,20,bIdle3,30)
[m\BehaviorSet\bAngry]
f = fPlaySetProb(bAngry1,50,bAngry2,30,bIdle3,20)
[m\BehaviorSet\bEscaped]
f = fPlaySetProb(bEscaped1,70,bNoise2,30)
[m\BehaviorSet\bBabyActions]
f = fPlaySetProb(Baby1,40,bBaby2,40,bIdle1,20)
[m\BehaviorSet\bPlayToys]
;f = fPlaySetProb(bToy3,20,bSpray1,20,bLog1,20,bToy1,20,bToy2,20)
[m\BehaviorSet\bPlayWithToyHappy]
;f = fPlaySetProb(bPlayToys,50,bIdle1,20,bHappy2,20,bNoise1,10)
[m\BehaviorSet\bPlayWithToyFriendly]
;f = fPlaySetProb(bPlayToy,50,bIdle2,30,bNoise2,20)
[m\BehaviorSet\bSoundtest1]
;f = fPlayWithSound(jerk_head,yip)
;f = fPlayWithSound(jerk_head,snort)
;f = fPlayWithSound(jerk_head,panic)
;f = fPlayWithSound(shake_head,snort)
;f = fPlayWithSound(rearup,yip)
;f = fPlayWithSound(rearup,snort)
;f = fPlayWithSound(rearup,panic)
;f = fPlayWithSound(buck,yip)
;f = fPlayWithSound(buck,panic)
[m\BehaviorSet\bHappy1]
f = fMove(0,0,jump_high,68)
f = fPlay(stand)
f = fPlayWithSound(jerk_head,snort)
f = fPlayTime(stand,4)
f = fPlay(head_ground)
f = fPlay(eat)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[m\BehaviorSet\bHappy2]
f = fPlay(stand)
f = fPlay(scuff_feet)
f = fMove(0,0,prance,32)
f = fPlay(stand)
f = fPlayWithSound(jerk_head,snort)
f = fPlay(stand)
[m\BehaviorSet\bHappy3]
f = fWalk(0,0)
f = fPlay(stand)
f = fPlayPingPong(look_right)
f = fPlayTime(stand,6)
f = fPlay(head_ground)
f = fPlayTime(eat,10)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[m\BehaviorSet\bCalm1]
f = fPlay(stand)
f = fPlayWithSound(jerk_head,snort)
f = fPlay(head_ground)
f = fPlayTime(eat,5)
f = fPlayReverse(head_ground)
f = fPlayPingPong(look_right)
f = fPlay(stand)
[m\BehaviorSet\bUneasy1]
f = fPlay(stamp_feet)
f = fMove(0,0,trot,42)
f = fPlayTime(stand,3)
f = fPlayWithSound(jerk_head,snort)
f = fPlay(stand)
[m\BehaviorSet\bTense1]
f = fPlay(stand)
f = fWalk(0,0)
f = fPlay(shoo_fly)
f = fPlay(stand)
[m\BehaviorSet\bAngry1]
f = fPlay(stand)
f = fPlayWithSound(rearup,panic)
f = fPlayTime(stand,7)
f = fPlayWithSound(buck,yip)
f = fMove(0,0,run,68)
f = fPlay(stand)
f = fPlay(stamp_feet)
f = fPlay(stand)
[m\BehaviorSet\bAngry2]
f = fMove(0,0,run,68)
f = fPlay(stand)
f = fPlayWithSound(buck,yip)
f = fPlayTime(stand,6)
f = fPlayWithSound(jerk_head,snort)
f = fPlay(stand)
[m\BehaviorSet\bAngry3]
f = fPlay(stand)
f = fPlayWithSound(buck,panic)
f = fPlayTime(stand,6)
f = fMove(0,0,run,68)
f = fPlay(stand)
f = fPlayWithSound(rearup,yip)
f = fPlay(stand)
[m\BehaviorSet\bNoise1]
f = fPlay(stand)
f = fPlayWithSound(jerk_head,yip)
f = fPlay(stand)
[m\BehaviorSet\bNoise2]
f = fPlay(stand)
f = fPlayWithSound(jerk_head,panic)
f = fPlay(stand)
[m\BehaviorSet\bIdle1]
f = fPlayWithSound(shake_head,yip)
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,24)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[m\BehaviorSet\bIdle2]
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,4)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[m\BehaviorSet\bIdle3]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,14)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[m\BehaviorSet\bSleep]
f = fPlay(lie_down)
f = fPlayTime(lie_idle,25)
f = fPlayReverse(lie_down)
[m\BehaviorSet\bDie]
f = fPlay(lie_down)
f = fPlayTime(lie_idle,5)
f = fPlayTime(lie_idle,5)
f = fDie()
[m\BehaviorSet\bFindKeeperFood]
f = fWalk(keeperfood,0)
f = fPlaySet(bEat)
[m\BehaviorSet\bFindOtherFood]
f = fWalk(otherfood,0)
f = fPlaySet(bEat)
[m\BehaviorSet\bEat]
f = fFaceTowardFood()
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,5)
f = fPlayReverse(head_ground)
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,4)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[m\BehaviorSet\bSick]
f = fPlay(lie_down)
f = fPlayTime(lie_idle,25)
f = fPlayReverse(lie_down)
[m\BehaviorSet\bEscaped1]
f = fPlay(stand)
f = fMove(0,0,run,68)
f = fPlay(stand)
f = fPlayWithSound(rearup,yip)
f = fPlay(stand)
[m\BehaviorSet\bChasePrey]
f = fMove(prey,0,"run",80)
[m\BehaviorSet\bRunFromPredator]
f = fRun(0,0)
[m\BehaviorSet\bCaught]
f = fFollow(keeper)
[m\BehaviorSet\bBaby1]
f = fPlay(stand)
f = fWalk(mother,0)
[m\BehaviorSet\bBaby2]
f = fPlay(stand)
f = fWalk(mother,0)
[m\BehaviorSet\bBored1]
f = fPlayTime(stand,10)
[m\BehaviorSet\bBored2]
f = fPlayTime(stand,10)
[m\BehaviorSet\bAngryKeeper]
f = fPlay(stand)
[m\BehaviorSet\bWalk]
f = fWalk(0,0)
[m\BehaviorSet\bRun]
f = fMove(0,0,run,68)
[m\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
[AmbientAnims]
;a = -100
;a = 100
;b = bSoundtest1
a = 81
a = 100
b = bHappy
a = 61
a = 80
b = bFriendly
a = 21
a = 60
b = bCalm
a = -20
a = 20
b = bNeutral
a = -60
a = -21
b = bUneasy
a = -80
a = -61
b = bTense
a = -100
a = -81
b = bAngry
