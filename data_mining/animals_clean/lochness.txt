[Global]
Class = animals
Type = lochness
Subtype = 
DefaultSubtype = m
[AnimPath]
m = animals/lochness/m
f = animals/lochness/m
y = animals/lochness/y
[m/Icon]
Icon = animals/lochness/iclochne/iclochne
[Characteristics/Integers]
cIconZoom = -2
; shadows for this unit
cUsesRealShadows = 1
cHasShadowImages = 1
cHasUnderwaterSection = 1
[Member]
dinosaur
animals
[f/Characteristics/Strings]
[m/Characteristics/Strings]
cKeeperFoodType = carnivore
cGeneralInfoTextName = LOCHNESS_GENERAL
cPlaqueImageName = animals/lochness/pllochne/pllochne
cListImageName = animals/lochness/lslochne/lslochne
cPrefIcon = objects/wtrlily/SE/SE;Water Lily
[f/Characteristics/Integers]
[m/Characteristics/Integers]
cKeeperFoodType = 5
cPrefIconID = 7061;Water Lily
cLaysEggs = 1
cEatsEggs = 1
cTimeToHatch = 160
cEggIconZoom = 0
cBoxedIconZoom = -2
cNameID = 5515;Loch Ness monster
cHelpID = 5515
cFamily = 5216; Dinosaurs
cGenus = 5148; Plesiosaurs
cHabitat = 9413;Aquatic
cLocation = 9613;Europe
cPurchaseCost = 2600
cInitialHappiness = 60
cSlowRate = 30
cMediumRate = 45
cFastRate = 60
cFootprintX = 4
cFootprintY = 8
cBoxFootprintX = 2
cBoxFootprintY = 2
cBoxFootprintZ = 2
cHeliRecovery=1
walkable = 0
cTall = 1
cBabiesAttack = 0
cPreattack = 0
cResetPreyPosition = 1
cCaptivity = 5
cNoFoodChange = -60
cSickChange = -10
cOtherAnimalSickChange = -10
cCrowdHappinessChange = -20
cOtherAnimalAngryChange = 0 
cNumberMinChange = -5
cNumberMaxChange = -10
cAllCrowdedChange = -5
cAngryHabitatChange = -10
cVeryAngryHabitatChange = -30
cDeathChange = -5 
cNotEnoughKeepersChange = -20
cAngryTreeChange = -10
cBabyBornChange = 50
cHappyHabitatChange = 20
cMaxHits = 225
cMinHits = 0
cPctHits = 20
cHungerThreshold = 50
cHungerIncrement = 10
cNeededFood = 100
cKeeperFoodUnitsEaten = 10
cHungryHealthChange = 10
cSickChance = 10
cEnergyThreshold = 100 
cEnergyIncrement = 10	
cMaxEnergy = 100
cCrowd = 25
cKeeperFrequency = 2
cKeeperArrivesChange = 1
cSocial = 0
cHabitatSize = 100
cNumberAnimalsMin = 1
cNumberAnimalsMax = 3
cAnimalDensity = 50
cPctHabitat = 10
cHabitatPreference = 80
cDirtyHabitatRating = 25
cDirtyIncrement = 10
cDirtyThreshold = 100
cPooWaterImpact = 5 
cMurkyWaterChange = -5
cMurkyWaterThreshold = 60
cVeryMurkyWaterChange = -10
cVeryMurkyWaterThreshold = 20
cExtremelyMurkyWaterChange = -15
cExtremelyMurkyWaterThreshold= 1
cReproductionChance = 1 
cReproductionInterval = 3600
cBabyToAdult = 2160
cHappyReproduceThreshold = 97
cOffspring = 1
cMatingType = 0
cNoMateChange = -5
cIsJumper = 0
cIsClimber = 0
cIsManEater = 0
cBashStrength = 240
cCrushesFences = 1
cKeeper = 9551
cPrey = 5039;Emperor Pengiun
cPrey = 5040;California Sea Lion
cPrey = 5041;Saltwater Crocodile
cPrey = 5402;Dolphin
cPrey = 5403;Arctic Lion's Mane Jellyfish
cPrey = 5404;Northern Elephant Seal
cPrey = 5408;Porpoise
cPrey = 5412;Moray Eel
cPrey = 5414;Sea Otter
cPrey = 5415;Octopus
cPrey = 5416;Manatee
cPrey = 5417;Walrus
cPrey = 5419;Sea Turtle
cPrey = 5420;Swordfish
cPrey = 5422;Bluefin Tuna
cPrey = 5423;Barracuda
cFlies = 0
cSwims = 1
cWaterNeeded = 8
cLandNeeded = 2
cEnterWaterChance = 90
cEnterLandChance = 10
cUnderwater = 1
cUnderwaterNeeded = 10
cEnterTankChance = 90
cEscapedChange = 30
cSickTime = 4
cMimic = 0
cMimicHappyDiff = 0
cOtherFood = 0
cFoodTypes = 0
cTreePref = 4
cRockPref = 6
cSpacePref = 70
cElevationPref = 0
cSalinityChange = -10
cSalinityHealthChange = -20
cTimeDeath = 12960
cDeathChance = 10
cEatVegetationChance = 0
cDrinkWaterChance = 0
cChaseAnimalChance = 10
cClimbsCliffs = 0
cDinoZoodoo = 1
cSmallZoodoo = 1
cBuildingUseChance = 0
cAttractiveness = 70
cNeedShelter = 0
cBreathThreshold  = 20
cBreathIncrement  = 10
[y/Characteristics/Integers]
cIconZoom = 0
cBoxedIconZoom = -2
cChaseAnimalChance = 15
cAttractiveness = 75
cFootprintX = 2
cFootprintY = 4
cSlowRate = 15
cMediumRate = 20
cFastRate = 25
cTall = 0
cEggFootprintX = 2
cEggFootprintY = 2
cEggFootprintZ = 2
[m/Characteristics/Mixed]
[cCompatibleAnimals]
v = 5200;Primitive ungulates
v = -20
v = 5201;Racoon
v = -10
v = 5202;Bears
v = -20
v = 5203;Primates
v = -5
v = 5204;Odd-toed ungulates
v = -10
v = 5205;Even-toed ungulates
v = -10
v = 5206;Cat
v = -20
v = 5207;Canine
v = -20
v = 5208;Hyena
v = -20
v = 5210;Marsupial
v = -5
v = 5211;Bird
v = -5
v = 5212;Seal
v = -15
v = 5213;Crocodile
v = -20
v = 5214;Edentate
v = -5
v = 5215;Pig
v = -5
v = 5135;Tyrannosaurs
v = -25
v = 5136;Smilodons
v = -10
v = 5137;Ankylosaurs
v = -10
v = 5138;Gallimimuses
v = -10
v = 5139;Iguanodons
v = -10
v = 5140;Lambeosaurs
v = -10
v = 5141;Spinosaurs
v = -25
v = 5142;Styracosaurs
v = -10
v = 5143;Velociraptors
v = -10
v = 5144;Allosaurs
v = -25
v = 5145;Camptosaurs
v = -10
v = 5146;Caudipteryxes
v = -10
v = 5147;Kentrosaurs
v = -10
v = 5148;Plesiosaurs
v = -10
v = 5149;Stegosaurs
v = -10
v = 5150;Apatosaurs
v = -10
v = 5151;Coelophysises
v = -10
v = 5152;Herrerasaurs
v = -10
v = 5153;Plateosaurs
v = -10
v = 5154;Wooly Mammoths
v = -10
v = 5155;Wooly Rhinos
v = -10
v = 5156;Meiolanias
v = -10
v = 5157;Triceratopses
v = -10
v = 5158;Deinosuchus
v = -25
v = 5159;Orcas
v = -10
v = 5160;Great White Sharks
v = -20
v = 5161;Bottlenose Dolphins
v = -10
v = 5162;Jellyfishes
v = -10
v = 5163;Northern Elephant Seals
v = -10
v = 5164;Humpback Whales
v = -10
v = 5165;Sperm Whales
v = -10
v = 5166;Narwhals
v = -10
v = 5167;Harbor Porpoises
v = -10
v = 5168;Hammerhead Sharks
v = -10
v = 5169;Tiger Sharks
v = -10
v = 5170;Mako Sharks
v = -20
v = 5171;Moray Eels
v = -10
v = 5172;Belugas
v = -10
v = 5173;Sea Otters
v = -10
v = 5174;Octopuses
v = -10
v = 5175;Manatees
v = -10
v = 5176;Walruses
v = -10
v = 5177;Manta Rays
v = -20
v = 5178;Sea Turtles
v = -10
v = 5179;Swordfishes
v = -10
v = 5180;Giant Squids
v = -20
v = 5181;Bluefin Tuna
v = -10
v = 5182;Baracudas
v = -10
v = 5183;Mermaids
v = 10
v = 5098;Plesiosaurs
v = 15
v = 5515;Loch Ness Monsters
v = 10
[cSuitableObjects]
v = 9410;Non-habitat
v = -10
v = 9405;Tropical rainforest
v = -10
v = 9400;Savannah
v = -10
v = 9401;Grassland
v = -10
v = 9402;Deciduous forest
v = -10
v = 9403;Coniferous forest
v = -10
v = 9407;Highlands
v = -10
v = 9408;Tundra
v = -5
v = 9409;Desert
v = -25
v = 9413;Aquatic
v = 10
v = 9414;Bad-habitat
v = -100
;FOLIAGE AND ROCKS
v = 9200;Large Rock 1
v = 5
v = 9201;Large Rock 2
v = 5
v = 9202;Large Rock 3
v = 5
v = 9203;Large Rock 4
v = 5
v = 9204;Large Rock 5
v = 5
v = 9205;Small Rock 1
v = 3
v = 9206;Small Rock 2
v = 3
v = 9207;Small Rock 3
v = 3
v = 9208;Small Rock 4
v = 3
v = 9209;Small Rock 5
v = 3
v = 6062;Poo
v = -10
v = 7061;Water Lily
v = 8
v = 7088;Horsetail
v = 6
v = 7083;Bald Cypress Tree
v = 7
;SHELTERS
[cCompatibleTerrain]
v = 16
v = -5
v = 2
v = 10
v = 6
v = -5
v = 5
v = 5
v = 4
v = -10
v = 9
v = 85
v = 10
v = -10
v = 13
v = -25
v = 15
v = -15
v = 14
v = -15
v = 0
v = -10
[m/Animations]
;From stand
stand = lieidle
idle = lieidle
lie_idle = lieidle
eat_down = eatdown
head_down = headdown
head_ground = headgrnd
nesting = nesting
sleep = sleep
;Movement
swim = surfswim
flop = flop
trot = flop
run = flop
walk = flop
;Tank Surface
surfidle = surfidle
surfswim = surfswim
head_raise = surfhead
look_left = surfleft
look_right = surfrigh
roar = surfroar
submerge = surfsubm
shake = surfshak
snort = surfsnor
swim_eat = surfeat
swim_eat_left = surfelft
swim_eat_right = surfergt
water_idle = surfidle
;Tank Surface Shadows
shadowsurfidle = ssurfidl
shadowsurfswim = ssurfswi
shadowsurfeat = ssurfeat
shadowsurfelft = ssurfelf
shadowsurfergt = ssurferg
shadowsurfhead = ssurfhea
shadowsurfidle = ssurfidl
shadowsurfleft = ssurflef
shadowsurfrigh = ssurfrig
shadowsurfroar = ssurfroa
shadowsurfshak = ssurfsha
shadowsurfsnor = ssurfsno
shadowsurfsubm = ssurfsub
;Tank Surface Undersides
undersurfidle = usurfidl
undersurfswim = usurfswi
undersurfeat = usurfeat
undersurfelft = usurfelf
undersurfergt = usurferg
undersurfhead = usurfhea
undersurfidle = usurfidl
undersurfleft = usurflef
undersurfrigh = usurfrig
undersurfroar = usurfroa
undersurfshak = usurfsha
undersurfsnor = usurfsno
undersurfsubm = usurfsub
;Tank Submerged
dive = subdive
rise = subrise
subidle = subswim
subswim = subswim
;Tank Submerged Shadows
shadowsubdive = ssubdive
shadowsubrise = ssubrise
shadowsubswim = ssubswim
;Box, Dustball and Egg animations
box_idle = objects/dinobox/idle/idle.ani
box_used = objects/dinobox/used/used.ani
dustball = objects/dustcl/medium/medium.ani
waterball = objects/wtrcloud/idle/idle.ani
surfaceball = objects/surfcld/surfc/surfc.ani
shadowsurfc = objects/surfcld/ssurfc/ssurfc.ani
undersurfc = objects/surfcld/usurfc/usurfc.ani
submergedball = objects/subcld/subc/subc.ani
shadowsubc = objects/subcld/ssubc/ssubc.ani
egg_idle = objects/e-plesio/idle/idle.ani
egg_hatch = objects/e-plesio/used/used.ani
egg_break = objects/e-plesio/break/break.ani
[y/Animations]
;From stand
stand = lieidle
idle = lieidle
lie_idle = lieidle
eat_down = eatdown
head_down = headdown
head_ground = headgrnd
nesting = nesting
sleep = sleep
;Movement
swim = surfswim
flop = flop
run = flop
trot = flop
walk = flop
;Tank Surface
surfidle = surfidle
surfswim = surfswim
head_raise = surfhead
look_left = surfleft
look_right = surfrigh
roar = surfroar
submerge = surfsubm
shake = surfshak
snort = surfsnor
swim_eat = surfeat
swim_eat_left = surfelft
swim_eat_right = surfergt
water_idle = surfidle
;Tank Surface Shadows
shadowsurfidle = ssurfidl
shadowsurfswim = ssurfswi
shadowsurfeat = ssurfeat
shadowsurfelft = ssurfelf
shadowsurfergt = ssurferg
shadowsurfhead = ssurfhea
shadowsurfidle = ssurfidl
shadowsurfleft = ssurflef
shadowsurfrigh = ssurfrig
shadowsurfroar = ssurfroa
shadowsurfshak = ssurfsha
shadowsurfsnor = ssurfsno
shadowsurfsubm = ssurfsub
;Tank Surface Undersides
undersurfidle = usurfidl
undersurfswim = usurfswi
undersurfeat = usurfeat
undersurfelft = usurfelf
undersurfergt = usurferg
undersurfhead = usurfhea
undersurfidle = usurfidl
undersurfleft = usurflef
undersurfrigh = usurfrig
undersurfroar = usurfroa
undersurfshak = usurfsha
undersurfsnor = usurfsno
undersurfsubm = usurfsub
;Tank Submerged
dive = subdive
rise = subrise
subidle = subswim
subswim = subswim
;Tank Submerged Shadows
shadowsubdive = ssubdive
shadowsubrise = ssubrise
shadowsubswim = ssubswim
;Box and Dustball animations
box_idle = objects/dinobox2/idle/idle.ani
box_used = objects/dinobox2/used/used.ani
dustball = objects/dustcl/small/small.ani
waterball = objects/wtrcloud/idle/idle.ani
surfaceball = objects/surfcld/surfc/surfc.ani
shadowsurfc = objects/surfcld/ssurfc/ssurfc.ani
undersurfc = objects/surfcld/usurfc/usurfc.ani
submergedball = objects/subcld/subc/subc.ani
shadowsubc = objects/subcld/ssubc/ssubc.ani
egg_idle = objects/e-plesio/idle/idle.ani
egg_hatch = objects/e-plesio/used/used.ani
egg_break = objects/e-plesio/break/break.ani
[Sounds]
;Male sounds
swimidle = animals/plesio/swimidle.wav
swimidle = 150
swim = animals/plesio/swim.wav
swim = 150
submerge = animals/plesio/submerge.wav
submerge = 150
snort = snort
snort = 150
roar = roar
roar = 150
dustball = roar
dustball = 300
placesound = roar
placesound = 1500
pickupsound = snort
pickupsound = 1500
hatch = sounds/egghatch.wav
hatch= 100
egg = animals/coelo/egg.wav
egg = 300
snore = sounds/sleep02.wav
snore = 100
;Loch Ness Monster Male Behavior Set Probabilities
[m\BehaviorSet\bHappy]
f = fPlaySetProb(bIdle1,20,bHappy1,50,bHappy2,30)
[m\BehaviorSet\bFriendly]
f = fPlaySetProb(bHappy3,70,bIdle2,30)
[m\BehaviorSet\bCalm]
f = fPlaySetProb(bCalm1,50,bIdle1,30,bIdle2,20)
[m\BehaviorSet\bNeutral]
f = fPlaySetProb(bIdle1,40,bIdle2,40,bNeutral1,20)
[m\BehaviorSet\bUneasy]
f = fPlaySetProb(bUneasy1,35,bIdle2,30,bUneasy2,35)
[m\BehaviorSet\bTense]
f = fPlaySetProb(bTense1,60,bAngry3,40)
[m\BehaviorSet\bAngry]
f = fPlaySetProb(bAngry1,50,bAngry2,50)
[m\BehaviorSet\bInWater]
f = fPlaySetProb(bWaterSwim,40,bWaterIdle,10,bWaterEat,5,bIdle3,10,bIdle4,10,bSubmerged,15,bNoise1,10)
[m\BehaviorSet\bEscaped]
f = fPlaySetProb(bEscaped1,100)
[m\BehaviorSet\bHappyU]
f = fPlaySetProb(bSubswim,80,bRise,10,bDive,10)
[m\BehaviorSet\bFriendlyU]
f = fPlaySetProb(bSubswim,75,bRise,10,bDive,10,bSurface,5)
[m\BehaviorSet\bCalmU]
f = fPlaySetProb(bSubswim,70,bRise,10,bSurface,10,bDive,10)
[m\BehaviorSet\bNeutralU]
f = fPlaySetProb(bSubswim,60,bRise,10,bSurface,10,bDive,10,bFloor,10)
[m\BehaviorSet\bUneasyU]
f = fPlaySetProb(bSubswim,70,bRise,5,bSurface,5,bDive,10,bFloor,10)
[m\BehaviorSet\bTenseU]
f = fPlaySetProb(bSubswim,55,bRise,5,bDive,20,bFloor,20)
[m\BehaviorSet\bAngryU]
f = fPlaySetProb(bSubswim,60,bRise,5,bDive,20,bFloor,15)
[m\BehaviorSet\bHappyS]
f = fPlaySetProb(bSurface,80,bDive,20)
[m\BehaviorSet\bFriendlyS]
f = fPlaySetProb(bSurface,85,bDive,15)
[m\BehaviorSet\bCalmS]
f = fPlaySetProb(bSurface,85,bDive,15)
[m\BehaviorSet\bNeutralS]
f = fPlaySetProb(bSurface,90,bDive,10)
[m\BehaviorSet\bUneasyS]
f = fPlaySetProb(bSurface,70,bDive,20,bFloor,10)
[m\BehaviorSet\bTenseS]
f = fPlaySetProb(bSurface,70,bDive,20,bFloor,10)
[m\BehaviorSet\bAngryS]
f = fPlaySetProb(bSurface,65,bDive,20,bFloor,15)
;Loch Ness Monster Male Behaviors
[m\BehaviorSet\bHappy1]
f = fPlay(lie_idle)
f = fPlay(nesting)
f = fPlayPingPong(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bHappy2]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bHappy3]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bCalm1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlayTime(lie_idle,5)
f = fPlay(nesting)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bUneasy1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bUneasy2]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bTense1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bAngry1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bAngry2]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bAngry3]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bNoise1]
f = fPlay(water_idle)
f = fPlayWithSound(roar,roar)
f = fPlay(water_idle)
[m\BehaviorSet\bIdle1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bIdle2]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(nesting)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bIdle3]
f = fPlay(water_idle)
f = fPlayWithSound(snort,snort)
f = fWalk(0,0)
f = fPlay(shake)
f = fWalk(0,0)
f = fPlayPingPong(look_left)
f = fPlay(water_idle)
[m\BehaviorSet\bIdle4]
f = fPlay(water_idle)
f = fPlayWithSound(roar,roar)
f = fPlayPingPong(look_right)
f = fPlay(head_raise)
f = fPlayWithSound(snort,snort)
f = fPlay(water_idle)
[m\BehaviorSet\bNeutral1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[m\BehaviorSet\bSurface]
f = fWalk(surface, 0)
[m\BehaviorSet\bDive]
f = fWalk(dive, 0)
[m\BehaviorSet\bRise]
f = fWalk(rise, 0)
[m\BehaviorSet\bSubswim]
f = fWalk(level, 0)
[m\BehaviorSet\bFloor]
f = fWalk(floor, 0)
[m\BehaviorSet\bBreathe]
f = fPlayTime(surfidle,3)
[m\BehaviorSet\bSleep]
f = fPlaySetTerrain(bSleepland,bSleepwater,bSleepsurf,bSleepsub)
[m\BehaviorSet\bSleepland]
f = fPlay(lie_idle)
f = fPlay(head_ground)
f = fPlayWithSound(sleep,snore)
f = fPlayTime(sleep,20)
f = fPlayReverse(head_ground)
f = fPlay(lie_idle)
[m\BehaviorSet\bSleepwater]
f = fPlayTime(water_idle,9)
[m\BehaviorSet\bSleepsurf]
f = fWalk(surface, 0)
[m\BehaviorSet\bSleepsub]
f = fWalk(level, 0)
[m\BehaviorSet\bDie]
f = fPlaySetTerrain(bDieland,bDiewater,bDiesurf,bDiesub)
[m\BehaviorSet\bDieland]
f = fPlay(lie_idle)
f = fPlay(head_ground)
f = fPlay(sleep)
f = fPlayTime(sleep,5)
f = fDie()
[m\BehaviorSet\bDiewater]
f = fPlayTime(water_idle,9)
f = fDie()
[m\BehaviorSet\bDiesurf]
f = fWalk(surface, 0)
f = fDie()
[m\BehaviorSet\bDiesub]
f = fWalk(level, 0)
f = fDie()
[m\BehaviorSet\bFindKeeperFood]
f = fWalk(keeperfood,0)
f = fPlaySet(bEat)
[m\BehaviorSet\bFindOtherFood]
f = fWalk(otherfood,0)
f = fPlaySet(bEat)
[m\BehaviorSet\bEat]
f = fFaceTowardFood()
f = fPlay(lie_idle)
f = fPlay(head_down)
f = fPlayTime(eat_down,9)
f = fPlayReverse(head_down)
f = fPlay(lie_idle)
[m\BehaviorSet\bSick]
f = fPlaySetTerrain(bSickland,bSickwater,bSicksurf,bSicksub)
[m\BehaviorSet\bSickland]
f = fPlay(lie_idle)
f = fPlay(head_ground)
f = fPlay(sleep)
f = fPlayTime(sleep,20)
f = fPlayReverse(head_ground)
f = fPlay(lie_idle)
[m\BehaviorSet\bSickwater]
f = fPlayTime(water_idle,5)
f = fPlayTime(water_idle,5)
[m\BehaviorSet\bSicksurf]
f = fWalk(surface, 0)
[m\BehaviorSet\bSicksub]
f = fWalk(level, 0)
[m\BehaviorSet\bEscaped1]
f = fRun(0,0)
[m\BehaviorSet\bChasePrey]
f = fRun(prey,0)
[m\BehaviorSet\bRunFromPredator]
f = fRun(0,0)
[m\BehaviorSet\bCaughtGuest]
f = fPlaySetTerrain(bcgland,bcgwater,bcgsurf,bcgsub)
[m\BehaviorSet\bcgland]
f = fPlayTime(stand,4)
[m\BehaviorSet\bcgwater]
f = fPlayTime(surfidle,4)
[m\BehaviorSet\bcgsurf]
f = fPlayTime(surfidle,4)
[m\BehaviorSet\bcgsub]
f = fPlayTime(subidle,4)
[m\BehaviorSet\bCaughtPrey]
f = fDustBall()
f = fPlaySetTerrain(bCalm1,bACwater,bAcsurf,bAcsub)
[m\BehaviorSet\bACwater]
f = fPlay(water_idle)
f = fPlayWithSound(roar,roar)
f = fPlay(water_idle)
[m\BehaviorSet\bAcsurf]
f = fWalk(surface, 0)
[m\BehaviorSet\bAcsub]
f = fWalk(level, 0)
[m\BehaviorSet\bCaught]
f = fFollow(keeper)
[m\BehaviorSet\bCaughtEgg]
f = fFaceTowardTarget()
f = fPlay(head_down)
f = fPlayWithSound(eat_down,egg)
f = fPlayTime(eat_down,8)
f = fPlayReverse(head_down)
f = fCaughtPrey()
f = fPlay(stand)
[m\BehaviorSet\bWalk]
f = fWalk(0,0)
[m\BehaviorSet\bRun]
f = fRun(0,0)
[m\BehaviorSet\bWaterSwim]
f = fWalk(0,0)
f = fPlayTime(water_idle,4)
[m\BehaviorSet\bWaterIdle]
f = fPlayTime(water_idle,4)
f = fWalk(0,0)
f = fPlayTime(water_idle,8)
[m\BehaviorSet\bWaterEat]
f = fPlay(water_idle)
f = fPlay(swim_eat)
f = fPlay(swim_eat_left)
f = fPlay(swim_eat_right)
f = fPlay(water_idle)
[m\BehaviorSet\bSubmerged]
f = fPlay(water_idle)
f = fPlayWithSound(submerge,submerge)
f = fPlayReverse(submerge)
f = fPlay(water_idle)
[m\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
;Loch Ness Monster Young Behavior Set Probabilities
[y\BehaviorSet\bHappy]
f = fPlaySetProb(bIdle1,20,bHappy1,50,bHappy2,30)
[y\BehaviorSet\bFriendly]
f = fPlaySetProb(bHappy3,70,bIdle2,30)
[y\BehaviorSet\bCalm]
f = fPlaySetProb(bCalm1,50,bIdle1,30,bIdle2,20)
[y\BehaviorSet\bNeutral]
f = fPlaySetProb(bIdle1,40,bIdle2,40,bNeutral1,20)
[y\BehaviorSet\bUneasy]
f = fPlaySetProb(bUneasy1,50,bIdle2,15,bUneasy2,35)
[y\BehaviorSet\bInWater]
f = fPlaySetProb(bWaterSwim,40,bWaterIdle,10,bWaterEat,10,bIdle3,5,bIdle4,10,bSubmerged,5,bNoise1,20)
[y\BehaviorSet\bTense]
f = fPlaySetProb(bTense1,60,bAngry3,40)
[y\BehaviorSet\bAngry]
f = fPlaySetProb(bAngry1,50,bAngry2,50)
[y\BehaviorSet\bEscaped]
f = fPlaySetProb(bEscaped1,100)
[y\BehaviorSet\bHappyU]
f = fPlaySetProb(bSubswim,80,bRise,10,bDive,10)
[y\BehaviorSet\bFriendlyU]
f = fPlaySetProb(bSubswim,75,bRise,10,bDive,10,bSurface,5)
[y\BehaviorSet\bCalmU]
f = fPlaySetProb(bSubswim,70,bRise,10,bSurface,10,bDive,10)
[y\BehaviorSet\bNeutralU]
f = fPlaySetProb(bSubswim,60,bRise,10,bSurface,10,bDive,10,bFloor,10)
[y\BehaviorSet\bUneasyU]
f = fPlaySetProb(bSubswim,70,bRise,5,bSurface,5,bDive,10,bFloor,10)
[y\BehaviorSet\bTenseU]
f = fPlaySetProb(bSubswim,55,bRise,5,bDive,20,bFloor,20)
[y\BehaviorSet\bAngryU]
f = fPlaySetProb(bSubswim,60,bRise,5,bDive,20,bFloor,15)
[y\BehaviorSet\bHappyS]
f = fPlaySetProb(bSurface,80,bDive,20)
[y\BehaviorSet\bFriendlyS]
f = fPlaySetProb(bSurface,85,bDive,15)
[y\BehaviorSet\bCalmS]
f = fPlaySetProb(bSurface,85,bDive,15)
[y\BehaviorSet\bNeutralS]
f = fPlaySetProb(bSurface,90,bDive,10)
[y\BehaviorSet\bUneasyS]
f = fPlaySetProb(bSurface,70,bDive,20,bFloor,10)
[y\BehaviorSet\bTenseS]
f = fPlaySetProb(bSurface,70,bDive,20,bFloor,10)
[y\BehaviorSet\bAngryS]
f = fPlaySetProb(bSurface,65,bDive,20,bFloor,15)
;Young Loch Ness Monster Behavior Sets
[y\BehaviorSet\bHappy1]
f = fPlay(lie_idle)
f = fPlay(nesting)
f = fPlayPingPong(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bHappy2]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bHappy3]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bCalm1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlayTime(lie_idle,5)
f = fPlay(nesting)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bUneasy1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bUneasy2]
f = fPlay(lie_idle)
f = fMove(0,0,flop,16)
f = fPlay(lie_idle)
[y\BehaviorSet\bTense1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bAngry1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bAngry2]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bAngry3]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bNoise1]
f = fPlay(water_idle)
f = fPlayWithSound(roar,roar)
f = fPlayWithSound(roar,roar)
f = fPlay(water_idle)
[y\BehaviorSet\bIdle1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bIdle2]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bIdle3]
f = fPlay(water_idle)
f = fPlayPingPong(look_right)
f = fWalk(0,0)
f = fPlay(shake)
f = fWalk(0,0)
f = fPlayWithSound(snort,snort)
f = fPlay(water_idle)
[y\BehaviorSet\bIdle4]
f = fPlay(water_idle)
f = fPlayWithSound(snort,snort)
f = fPlayWithSound(roar,roar)
f = fPlay(head_raise)
f = fPlayPingPong(look_right)
f = fPlay(water_idle)
[y\BehaviorSet\bNeutral1]
f = fPlay(lie_idle)
f = fWalk(0,0)
f = fPlay(lie_idle)
[y\BehaviorSet\bSurface]
f = fWalk(surface, 0)
[y\BehaviorSet\bDive]
f = fWalk(dive, 0)
[y\BehaviorSet\bRise]
f = fWalk(rise, 0)
[y\BehaviorSet\bSubswim]
f = fWalk(level, 0)
[y\BehaviorSet\bFloor]
f = fWalk(floor, 0)
[y\BehaviorSet\bSleep]
f = fPlaySetTerrain(bSleepland,bSleepwater,bSleepsurf,bSleepsub)
[y\BehaviorSet\bSleepland]
f = fPlay(lie_idle)
f = fPlay(head_ground)
f = fPlayWithSound(sleep,snore)
f = fPlayTime(sleep,20)
f = fPlayReverse(head_ground)
f = fPlay(lie_idle)
[y\BehaviorSet\bSleepwater]
f = fPlayTime(water_idle,9)
[y\BehaviorSet\bSleepsurf]
f = fWalk(surface, 0)
[y\BehaviorSet\bSleepsub]
f = fWalk(level, 0)
[y\BehaviorSet\bDie]
f = fPlaySetTerrain(bDieland,bDiewater,bDiesurf,bDiesub)
[y\BehaviorSet\bDieland]
f = fPlay(lie_idle)
f = fPlay(head_ground)
f = fPlay(sleep)
f = fPlayTime(sleep,5)
f = fDie()
[y\BehaviorSet\bDiewater]
f = fPlayTime(water_idle,9)
f = fDie()
[y\BehaviorSet\bDiesurf]
f = fWalk(surface, 0)
f = fDie()
[y\BehaviorSet\bDiesub]
f = fWalk(level, 0)
f = fDie()
[y\BehaviorSet\bFindKeeperFood]
f = fWalk(keeperfood,0)
f = fPlaySet(bEat)
[y\BehaviorSet\bFindOtherFood]
f = fWalk(otherfood,0)
f = fPlaySet(bEat)
[y\BehaviorSet\bEat]
f = fFaceTowardFood()
f = fPlay(lie_idle)
f = fPlay(head_down)
f = fPlayTime(eat_down,9)
f = fPlayReverse(head_down)
f = fPlay(lie_idle)
[y\BehaviorSet\bSick]
f = fPlaySetTerrain(bSickland,bSickwater,bSicksurf,bSicksub)
[y\BehaviorSet\bSickland]
f = fPlay(lie_idle)
f = fPlay(head_ground)
f = fPlay(sleep)
f = fPlayTime(sleep,20)
f = fPlayReverse(head_ground)
f = fPlay(lie_idle)
[y\BehaviorSet\bSickwater]
f = fPlayTime(water_idle,5)
f = fPlayTime(water_idle,5)
[y\BehaviorSet\bSicksurf]
f = fWalk(surface, 0)
[y\BehaviorSet\bSicksub]
f = fWalk(level, 0)
[y\BehaviorSet\bEscaped1]
f = fTrot(0,0)
[y\BehaviorSet\bChasePrey]
f = fTrot(prey,0)
[y\BehaviorSet\bRunFromPredator]
f = fTrot(0,0)
[y\BehaviorSet\bCaughtGuest]
f = fPlaySetTerrain(bcgland,bcgwater,bcgsurf,bcgsub)
[y\BehaviorSet\bcgland]
f = fPlayTime(stand,4)
[y\BehaviorSet\bcgwater]
f = fPlayTime(surfidle,4)
[y\BehaviorSet\bcgsurf]
f = fPlayTime(surfidle,4)
[y\BehaviorSet\bcgsub]
f = fPlayTime(subidle,4)
[y\BehaviorSet\bCaughtPrey]
f = fDustBall()
f = fPlaySetTerrain(bCalm1,bACwater,bAcsurf,bAcsub)
[y\BehaviorSet\bACwater]
f = fPlay(water_idle)
f = fPlayWithSound(roar,roar)
f = fPlay(water_idle)
[y\BehaviorSet\bAcsurf]
f = fWalk(surface, 0)
[y\BehaviorSet\bAcsub]
f = fWalk(level, 0)
[y\BehaviorSet\bCaught]
f = fFollow(keeper)
[y\BehaviorSet\bWalk]
f = fWalk(0,0)
[y\BehaviorSet\bRun]
f = fTrot(0,0)
[y\BehaviorSet\bWaterSwim]
f = fTrot(0,0)
f = fPlayTime(water_idle,4)
[y\BehaviorSet\bWaterIdle]
f = fPlayTime(water_idle,4)
f = fWalk(0,0)
f = fPlayTime(water_idle,8)
[y\BehaviorSet\bWaterEat]
f = fPlay(water_idle)
f = fPlay(swim_eat_right)
f = fPlay(swim_eat)
f = fPlay(swim_eat_left)
f = fPlay(water_idle)
[y\BehaviorSet\bSubmerged]
f = fPlay(water_idle)
f = fPlayWithSound(submerge,submerge)
f = fPlayReverse(submerge)
f = fPlay(water_idle)
[y\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
[y\BehaviorSet\bHatch]
f = fPlayWithSound(egg_hatch,hatch)
f = fPlay(idle)
f = fHatch()
[y\BehaviorSet\bEatenEgg]
f = fPlayStopAtEnd(egg_break)
[y\BehaviorSet\bCaughtEgg]
f = fFaceTowardTarget()
f = fPlay(head_down)
f = fPlayWithSound(eat_down,egg)
f = fPlayTime(eat_down,8)
f = fPlayReverse(head_down)
f = fCaughtPrey()
f = fPlay(stand)
[AmbientAnims]
a = 81
a = 100
b = bHappy
a = 61
a = 80
b = bFriendly
a = 21
a = 60
b = bCalm
a = -20
a = 20
b = bNeutral
a = -60
a = -21
b = bUneasy
a = -80
a = -61
b = bTense
a = -100
a = -81
b = bAngry
[AmbientAnimsWater]
a = -100
a = 100
b = bInWater
[AmbientAnimsUnderwater]
a = 81
a = 100
b = bHappyU
a = 61
a = 80
b = bFriendlyU
a = 21
a = 60
b = bCalmU
a = -20
a = 20
b = bNeutralU
a = -60
a = -21
b = bUneasyU
a = -80
a = -61
b = bTenseU
a = -100
a = -81
b = bAngryU
[AmbientAnimsSurface]
a = 81
a = 100
b = bHappyS
a = 61
a = 80
b = bFriendlyS
a = 21
a = 60
b = bCalmS
a = -20
a = 20
b = bNeutralS
a = -60
a = -21
b = bUneasyS
a = -80
a = -61
b = bTenseS
a = -100
a = -81
b = bAngryS