[Global]
Class = animals
Type = plateo
Subtype = 
DefaultSubtype = m
[AnimPath]
m = animals/plateo/m
f = animals/plateo/m
y = animals/plateo/y
[m/Icon]
Icon = animals/plateo/icplateo/icplateo
[Characteristics/Integers]
cIconZoom = -2
cExpansionID=1
[Member]
dinosaur
animals
[f/Characteristics/Strings]
[m/Characteristics/Strings]
cKeeperFoodType = herbivore
cGeneralInfoTextName = PLATEO_GENERAL
cPlaqueImageName = animals/plateo/plplateo/plplateo
cListImageName = animals/plateo/lsplateo/lsplateo
cPrefIcon = objects/glossop/SE/SE; Glossopteris
[f/Characteristics/Integers]
[m/Characteristics/Integers]
cKeeperFoodType = 8
cPrefIconID = 7087;Glossopteris
cLaysEggs = 1
cEatsEggs = 0
cTimeToHatch = 160
cEggIconZoom = 0
cBoxedIconZoom = -2
cNameID = 5328; Plateosaurus
cHelpID = 5328
cFamily = 5216; Dinosaurs
cGenus = 5153; Plateosaurs
cHabitat = 9402; Deciduous Forest
cLocation = 9613; Europe
cEra = 9620; Triassic
cPurchaseCost = 2600
cInitialHappiness  = 75
cSlowRate = 58
cMediumRate = 79
cFastRate = 100
cFootprintX = 2
cFootprintY = 8
cBoxFootprintX = 4
cBoxFootprintY = 4
cBoxFootprintZ = 4
cHeliRecovery=1
walkable = 0
cTall = 1
cBabiesAttack = 0;//Angry Effects//
cPreattack = 1
cResetPreyPosition = 1
cCaptivity = 7
cNoFoodChange  = -60
cSickChange = -10	
cOtherAnimalSickChange = -10		
cCrowdHappinessChange = -20
cOtherAnimalAngryChange = 0 
cNumberMinChange = -5
cNumberMaxChange = -10
cAllCrowdedChange = -5
cAngryHabitatChange = -10
cVeryAngryHabitatChange = -30
cDeathChange = -5 
cNotEnoughKeepersChange = -20
cAngryTreeChange = -10
cBabyBornChange = 50;//Happy Effects//
cHappyHabitatChange = 20
cMaxHits  = 225
cMinHits  = 0
cPctHits  = 20
cHungerThreshold  = 50
cHungerIncrement  = 10
cNeededFood = 100
cKeeperFoodUnitsEaten = 10
cHungryHealthChange = 10
cSickChance = 20
cEnergyThreshold = 100 
cEnergyIncrement  = 10	
cMaxEnergy = 100
cCrowd = 35
cKeeperFrequency = 2
cKeeperArrivesChange = 1
cSocial = 0
cHabitatSize = 100
cNumberAnimalsMin = 1
cNumberAnimalsMax = 3
cAnimalDensity = 100
cPctHabitat = 10
cHabitatPreference = 85
cDirtyHabitatRating = 25
cDirtyIncrement = 10
cDirtyThreshold = 100
cReproductionChance = 1
cReproductionInterval = 3240
cBabyToAdult = 2160
cHappyReproduceThreshold = 97
cOffspring = 1
cMatingType = 0
cNoMateChange = -5
cIsJumper = 1
cIsClimber = 0
cIsManEater = 0
cBashStrength = 270
cKeeper = 9551;Keeper
cPrey = 9503;Man
cPrey = 9551;Scientist
cPrey = 9500;Zookeeper
cPrey = 9501;Maintenance Worker
cPrey = 9502;Tour Guide
cFlies = 0
cSwims = 0
cEscapedChange = 40
cSickTime = 4
cMimic = 0
cMimicHappyDiff = 0
cOtherFood = 0
cFoodTypes = 0
cTreePref = 6
cRockPref = 2
cSpacePref = 50
cElevationPref = 0
cTimeDeath = 8640
cDeathChance = 10 
cEatVegetationChance = 20
cDrinkWaterChance = 5
cChaseAnimalChance = 10
cClimbsCliffs = 1
cDinoZoodoo = 1
cBuildingUseChance = 20
cAttractiveness = 60
cNeedShelter = 1
[y/Characteristics/Integers]
cPrefIconID = 7087
cChaseAnimalChance = 15
cIsJumper = 0
cAttractiveness = 75
cFootprintX = 2
cFootprintY = 2
cSlowRate = 29
cMediumRate = 40
cFastRate = 50
cTall = 0
cEggFootprintX = 2
cEggFootprintY = 2
cEggFootprintZ = 2
cBoxedIconZoom = -2
[m/Characteristics/Mixed]
[cCompatibleAnimals]
v = 5200;Primitive ungulates		
v = -20
v = 5201;Racoon			
v = -10
v = 5202;Bears			
v = -20
v = 5203;Primates			
v = -5
v = 5204;Odd-toed ungulates		
v = -10
v = 5205;Even-toed ungulates			
v = -10
v = 5206;Cat			
v = -20
v = 5207;Canine			
v = -20
v = 5208;Hyena			
v = -20
v = 5210;Marsupial		
v = -5
v = 5211;Bird			
v = -5
v = 5212;Seal			
v = -15
v = 5213;Crocodile		
v = -20
v = 5214;Edentate		    	
v = -5
v = 5215;Pig			
v = -5
v = 5135;Tyrannosaurs
v = -25
v = 5136;Smilodons
v = -10
v = 5137;Ankylosaurs
v = -10
v = 5138;Gallimimuses
v = -10
v = 5139;Iguanodons
v = -10
v = 5140;Lambeosaurs
v = -10
v = 5141;Spinosaurs
v = -25
v = 5142;Styracosaurs
v = -10
v = 5143;Velociraptors
v = -10
v = 5144;Allosaurs
v = -25
v = 5145;Camptosaurs
v = -10
v = 5146;Caudipteryxes
v = -10
v = 5147;Kentrosaurs
v = -10
v = 5148;Plesiosaurs
v = -10
v = 5149;Stegosaurs
v = -10
v = 5150;Apatosaurs
v = -10
v = 5151;Coelophysises
v = -10
v = 5152;Herrerasaurs
v = -10
v = 5153;Plateosaurs
v = -10
v = 5154;Wooly Mammoths
v = -10
v = 5155;Wooly Rhinos
v = -10
v = 5156;Meiolanias
v = -10
v = 5157;Triceratopses
v = -10
v = 5158;Deinosuchus
v = -25
v = 5328;Plateosaurus
v = 10
v = 5092;Styracosaurus
v = 15
v = 5089;Iguanodon
v = 15
[cSuitableObjects]
v = 9410;Non-habitat
v = -10
v = 9405;Tropical rainforest
v = -10
v = 9400;Savannah
v = -5
v = 9401;Grassland
v = 5
v = 9402;Deciduous forest
v = 10
v = 9403;Coniferous forest
v = -10
v = 9407;Highlands
v = -3
v = 9408;Tundra
v = -5
v = 9409;Desert
v = -25
v = 9413;Aquatic
v = -10
v = 9414;Bad-habitat
v = -100
;FOLIAGE AND ROCKS
v = 9223;Deciduous Rock Formation
v = 15
v = 9200;Large Rock 1
v = 5
v = 9201;Large Rock 2
v = 5
v = 9202;Large Rock 3
v = 5
v = 9203;Large Rock 4
v = 5
v = 9204;Large Rock 5
v = 5
v = 9205;Small Rock 1
v = 3
v = 9206;Small Rock 2
v = 3
v = 9207;Small Rock 3
v = 3
v = 9208;Small Rock 4
v = 3
v = 9209;Small Rock 5
v = 3
v = 6062;Poo
v = -10
v = 7091;Magnolia Tree
v = 15
v = 7086;Gingkos
v = 12
v = 7087;Glossopteris
v = 20
;SHELTERS
v = 8128;Dino Cave
v = 15
v = 8129;Dino Nest
v = 12
[cCompatibleTerrain]
v = 16
v = -5
v = 2
v = -5
v = 6
v = -5
v = 4
v = -10
v = 9
v = 10
v = 10
v = -25
v = 11
v = 85
v = 13
v = -10
v = 15
v = -15
v = 14
v = -15
v = 0
v = 5
[m/Animations]
;From stand
idle = stand
stand = stand
eat_down = eatdown
eat_up = eatup
eat_up_rear = eatupr
front_2_back = frnt2bck
head_down = headdown
head_ground = headgrnd
head_raise = headrais
head_up = headup
head_up_rear = headupr
lie_down = liedown
look_left = lookl
look_right = lookr
roar = roar
shake = shake
snort = snort
stand_rear = stndrear
stomp = stomp
;Movement
walk = walk
trot = trot
;Lying Down
lie_idle = lieidle
sleep = sleep
;Box and Dustball animations
box_idle = objects/dinobox/idle/idle.ani
box_used = objects/dinobox/used/used.ani
dustball = objects/dustcl/large/large.ani
egg_idle = objects/e-plateo/idle/idle.ani
egg_hatch = objects/e-plateo/used/used.ani
egg_break = objects/e-plateo/break/break.ani
[y/Animations]
;From stand
idle = stand
stand = stand
eat_down = eatdown
eat_up = eatup
eat_up_rear = eatupr
front_2_back = frnt2bck
head_down = headdown
head_ground = headgrnd
head_raise = headrais
head_up = headup
head_up_rear = headupr
lie_down = liedown
look_left = lookl
look_right = lookr
roar = roar
shake = shake
snort = snort
stand_rear = stndrear
stomp = stomp
;Movement
walk = walk
trot = trot
;Lying Down
lie_idle = lieidle
sleep = sleep
;Box and Dustball animations
box_idle = objects/dinobox/idle/idle.ani
box_used = objects/dinobox/used/used.ani
dustball = objects/dustcl/medium/medium.ani
egg_idle = objects/e-plateo/idle/idle.ani
egg_hatch = objects/e-plateo/used/used.ani
egg_break = objects/e-plateo/break/break.ani
[Sounds]
;Male sounds
roar = animals/plateo/roar.wav
roar = 150
stomp = animals/plateo/stomp.wav
stomp = 150
snort = animals/plateo/snort.wav
snort = 150
;Baby sounds go here
;dustball, place and pickup
dustball = animals/plateo/roar.wav
dustball = 300
placesound = animals/plateo/roar.wav
placesound = 1500
pickupsound = animals/plateo/snort.wav
pickupsound = 1500
hatch = sounds/egghatch.wav
hatch= 100
snore = sounds/sleep02.wav
snore = 100
;Camptosaurus Male Behavior Set Probabilities
[m\BehaviorSet\bHappy]
f = fPlaySetProb(bIdle1,20,bHappy1,50,bHappy2,30)
[m\BehaviorSet\bFriendly]
f = fPlaySetProb(bHappy3,70,bIdle2,30)
[m\BehaviorSet\bCalm]
f = fPlaySetProb(bCalm1,50,bIdle1,30,bIdle2,20)
[m\BehaviorSet\bNeutral]
f = fPlaySetProb(bIdle1,40,bIdle2,40,bNeutral1,20)
[m\BehaviorSet\bUneasy]
f = fPlaySetProb(bUneasy1,35,bNoise1,15,bIdle2,15,bUneasy2,35)
[m\BehaviorSet\bTense]
f = fPlaySetProb(bTense1,60,bAngry3,20,bNoise1,20)
[m\BehaviorSet\bAngry]
f = fPlaySetProb(bAngry1,50,bAngry2,50)
[m\BehaviorSet\bEscaped]
f = fPlaySetProb(bEscaped1,70,bNoise2,30)
;[m\BehaviorSet\bTest]
;f = fPlaySetProb(bWalk,50,bRun,50)
;Plateosaurus Male Behaviors
[m\BehaviorSet\bHappy1]
f = fPlay(stand)
f = fPlayWithSound(snort,snort)
f = fPlayPingPong(look_left)
f = fPlayWithSound(roar,roar)
f = fPlayPingPong(look_right)
f = fPlay(stand)
f = fTrot(0,0)
f = fPlay(stand)
[m\BehaviorSet\bHappy2]
f = fPlay(stand)
f = fPlay(front_2_back)
f = fPlayTime(stand_rear,5)
f = fPlayReverse(front_2_back)
f = fPlay(stand)
[m\BehaviorSet\bHappy3]
f = fPlay(stand)
f = fPlay(front_2_back)
f = fPlay(stand_rear)
f = fPlay(head_up_rear)
f = fPlayTime(eat_up_rear,9)
f = fPlayReverse(head_up_rear)
f = fPlayReverse(front_2_back)
f = fPlay(stand)
[m\BehaviorSet\bCalm1]
f = fPlay(stand)
f = fPlayWithSound(roar,roar)
f = fPlayTime(stand,5)
f = fPlayPingPong(head_raise)
f = fPlayPingPong(look_left)
f = fPlayPingPong(look_right)
f = fPlay(stand)
[m\BehaviorSet\bUneasy1]
f = fPlay(stand)
f = fPlayPingPong(look_left)
f = fPlayWithSound(snort,snort)
f = fPlayPingPong(look_right)
f = fPlay(shake)
f = fPlay(stand)
[m\BehaviorSet\bUneasy2]
f = fPlay(stand)
f = fWalk(0,0)
f = fPlay(shake)
f = fPlayWithSound(roar,roar)
f = fPlay(stand)
[m\BehaviorSet\bTense1]
f = fPlay(stand)
f = fPlay(shake)
f = fPlayWithSound(stomp,stomp)
f = fPlayWithSound(snort,snort)
f = fPlayWithSound(roar,roar)
f = fPlay(shake)
f = fPlay(stand)
[m\BehaviorSet\bAngry1]
f = fPlay(stand)
f = fPlayPingPong(head_raise)
f = fPlayWithSound(roar,roar)
f = fPlayWithSound(snort,snort)
f = fPlayWithSound(stomp,stomp)
f = fPlayWithSound(stomp,stomp)
f = fPlay(stand)
[m\BehaviorSet\bAngry2]
f = fPlay(stand)
f = fPlay(front_2_back)
f = fPlayTime(stand_rear,5)
f = fPlayPingPong(head_up_rear)
f = fPlayReverse(front_2_back)
f = fPlay(stand)
[m\BehaviorSet\bAngry3]
f = fPlay(stand)
f = fPlayWithSound(roar,roar)
f = fPlayWithSound(stomp,stomp)
f = fPlayWithSound(roar,roar)
f = fPlayWithSound(snort,snort)
f = fPlayWithSound(stomp,stomp)
f = fPlay(stand)
[m\BehaviorSet\bNoise1]
f = fPlay(stand)
f = fPlayWithSound(roar,roar)
f = fPlay(stand)
[m\BehaviorSet\bNoise2]
f = fPlay(stand)
f = fPlayWithSound(snort,snort)
f = fPlay(stand)
[m\BehaviorSet\bIdle1]
f = fPlay(stand)
f = fTrot(0,0)
f = fPlayWithSound(snort,snort)
f = fTrot(0,0)
f = fPlay(stand)
[m\BehaviorSet\bIdle2]
f = fPlay(stand)
f = fPlayPingPong(look_left)
f = fTrot(0,0)
f = fPlayWithSound(snort,snort)
f = fPlay(stand)
[m\BehaviorSet\bIdle3]
f = fPlay(stand)
f = fPlayWithSound(snort,snort)
f = fTrot(0,0)
f = fPlay(stand)
[m\BehaviorSet\bNeutral1]
f = fPlay(stand)
f = fPlayWithSound(shake,shake)
f = fWalk(0,0)
f = fPlay(stand)
f = fPlayWithSound(snort,snort)
f = fPlay(stand)
[m\BehaviorSet\bSleep]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,5)
f = fPlay(head_ground)
f = fPlayWithSound(sleep,snore)
f = fPlayTime(sleep,20)
f = fPlayReverse(head_ground)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[m\BehaviorSet\bDie]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlay(head_ground)
f = fPlay(sleep)
f = fPlayTime(sleep,5)
f = fDie()
[m\BehaviorSet\bPreattack]
f = fPlay(stand)
f = fPlayWithSound(roar,roar)
f = fPlay(stand)
[m\BehaviorSet\bDefense]
f = fPlayWithSound(stomp,stomp)
f = fPlayWithSound(roar,roar)
[m\BehaviorSet\bFindKeeperFood]
f = fTrot(keeperfood,0)
f = fPlaySet(bEat)
[m\BehaviorSet\bFindOtherFood]
f = fTrot(otherfood,0)
f = fPlaySet(bEat)
[m\BehaviorSet\bEat]
f = fFaceTowardFood()
f = fPlay(stand)
f = fPlay(head_down)
f = fPlayTime(eat_down,9)
f = fPlayReverse(head_down)
f = fPlay(stand)
[m\BehaviorSet\bSick]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlay(head_ground)
f = fPlay(sleep)
f = fPlayTime(sleep,5)
f = fPlayReverse(head_ground)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[m\BehaviorSet\bEscaped1]
f = fTrot(0,0)
[m\BehaviorSet\bChasePrey]
f = fTrot(prey,0)
[m\BehaviorSet\bRunFromPredator]
f = fTrot(0,0)
[m\BehaviorSet\bCaughtGuest]
; the time of animation below should match bCaughtBy in guest
f = fPlayTime(stand,4)
[m\BehaviorSet\bCaughtPrey]
f = fDustBall()
f = fPlaySet(bAfterCaught)
[m\BehaviorSet\bAfterCaught]
f = fPlay(stand)
f = fPlay(shake)
f = fPlayWithSound(roar,roar)
f = fPlay(stand)
[m\BehaviorSet\bCaught]
f = fFollow(keeper)
[m\BehaviorSet\bWalk]
f = fWalk(0,0)
[m\BehaviorSet\bRun]
f = fTrot(0,0)
[m\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
;Camptosaurus Young Behavior Set Probabilities
[y\BehaviorSet\bHappy]
f = fPlaySetProb(bIdle1,20,bHappy1,50,bHappy2,30)
[y\BehaviorSet\bFriendly]
f = fPlaySetProb(bHappy3,70,bIdle2,30)
[y\BehaviorSet\bCalm]
f = fPlaySetProb(bCalm1,50,bIdle1,30,bIdle2,20)
[y\BehaviorSet\bNeutral]
f = fPlaySetProb(bIdle1,40,bIdle2,40,bNeutral1,20)
[y\BehaviorSet\bUneasy]
f = fPlaySetProb(bUneasy1,35,bNoise1,15,bIdle2,15,bUneasy2,35)
[y\BehaviorSet\bTense]
f = fPlaySetProb(bTense1,60,bAngry3,20,bNoise1,20)
[y\BehaviorSet\bAngry]
f = fPlaySetProb(bAngry1,50,bAngry2,50)
[y\BehaviorSet\bEscaped]
f = fPlaySetProb(bEscaped1,70,bNoise2,30)
;Young Camptosaurus Behavior Sets
[y\BehaviorSet\bHappy1]
f = fPlay(stand)
f = fPlayPingPong(look_left)
f = fPlayWithSound(snort,snort)
f = fPlayPingPong(look_right)
f = fTrot(0,0)
f = fPlay(stand)
f = fPlayWithSound(roar,roar)
f = fPlay(stand)
[y\BehaviorSet\bHappy2]
f = fPlay(stand)
f = fPlay(front_2_back)
f = fPlayTime(stand_rear,5)
f = fPlayReverse(front_2_back)
f = fPlay(stand)
[y\BehaviorSet\bHappy3]
f = fPlay(stand)
f = fPlay(front_2_back)
f = fPlayTime(stand_rear,5)
f = fPlay(head_up_rear)
f = fPlayTime(eat_up_rear,9)
f = fPlayReverse(head_up_rear)
f = fPlayReverse(front_2_back)
f = fPlay(stand)
[y\BehaviorSet\bCalm1]
f = fPlay(stand)
f = fPlayPingPong(head_raise)
f = fPlayTime(stand,5)
f = fPlayWithSound(roar,roar)
f = fPlayPingPong(look_right)
f = fPlayWithSound(snort)
f = fPlay(stand)
[y\BehaviorSet\bUneasy1]
f = fPlay(stand)
f = fPlayPingPong(look_right)
f = fPlay(shake)
f = fPlayWithSound(snort,snort)
f = fPlayPingPong(look_left)
f = fPlay(stand)
[y\BehaviorSet\bUneasy2]
f = fPlay(stand)
f = fPlay(shake)
f = fPlayWithSound(roar,roar)
f = fWalk(0,0)
f = fPlay(stand)
[y\BehaviorSet\bTense1]
f = fPlay(stand)
f = fPlayWithSound(stomp,stomp)
f = fPlay(shake)
f = fPlayWithSound(roar,roar)
f = fPlay(shake)
f = fPlayWithSound(snort,snort)
f = fPlay(stand)
[y\BehaviorSet\bAngry1]
f = fPlay(stand)
f = fPlayWithSound(stomp,stomp)
f = fPlayPingPong(head_raise)
f = fPlayWithSound(stomp,stomp)
f = fPlayWithSound(roar,roar)
f = fPlayWithSound(snort,snort)
f = fPlay(stand)
[y\BehaviorSet\bAngry2]
f = fPlay(stand)
f = fPlay(front_2_back)
f = fPlayTime(stand_rear,3)
f = fPlayReverse(front_2_back)
f = fPlayWithSound(roar,roar)
f = fPlayTime(stand,2)
f = fPlay(front_2_back)
f = fPlayTime(stand_rear,3)
f = fPlayReverse(front_2_back)
f = fPlay(stand)
[y\BehaviorSet\bAngry3]
f = fPlay(stand)
f = fPlayWithSound(stomp,stomp)
f = fPlayWithSound(roar,roar)
f = fPlayWithSound(snort,snort)
f = fPlayWithSound(stomp,stomp)
f = fPlayWithSound(roar,roar)
f = fPlay(stand)
[y\BehaviorSet\bNoise1]
f = fPlay(stand)
f = fPlayWithSound(roar,roar)
f = fPlay(stand)
[y\BehaviorSet\bNoise2]
f = fPlay(stand)
f = fPlayWithSound(snort,snort)
f = fPlay(stand)
[y\BehaviorSet\bIdle1]
f = fPlay(stand)
f = fTrot(0,0)
f = fPlayPingPong(head_raise)
f = fTrot(0,0)
f = fPlay(stand)
[y\BehaviorSet\bIdle2]
f = fPlay(stand)
f = fPlayWithSound(snort,snort)
f = fTrot(0,0)
f = fPlayPingPong(look_left)
f = fPlay(stand)
[y\BehaviorSet\bIdle3]
f = fPlay(stand)
f = fTrot(0,0)
f = fPlayWithSound(snort,snort)
f = fPlay(stand)
[y\BehaviorSet\bNeutral1]
f = fPlay(stand)
f = fWalk(0,0)
f = fPlayWithSound(snort,snort)
f = fPlay(stand)
f = fPlayWithSound(shake,shake)
f = fPlay(stand)
[y\BehaviorSet\bSleep]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,5)
f = fPlay(head_ground)
f = fPlayWithSound(sleep,snore)
f = fPlayTime(sleep,20)
f = fPlayReverse(head_ground)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[y\BehaviorSet\bDie]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlay(head_ground)
f = fPlay(sleep)
f = fPlayTime(sleep,5)
f = fDie()
[y\BehaviorSet\bPreattack]
f = fPlay(stand)
f = fPlayWithSound(roar,roar)
f = fPlay(stand)
[y\BehaviorSet\bDefense]
f = fPlayWithSound(roar,roar)
f = fPlayWithSound(stomp,stomp)
[y\BehaviorSet\bFindKeeperFood]
f = fTrot(keeperfood,0)
f = fPlaySet(bEat)
[y\BehaviorSet\bFindOtherFood]
f = fTrot(otherfood,0)
f = fPlaySet(bEat)
[y\BehaviorSet\bEat]
f = fFaceTowardFood()
f = fPlay(stand)
f = fPlay(head_down)
f = fPlayTime(eat_down,9)
f = fPlayReverse(head_down)
f = fPlay(stand)
[y\BehaviorSet\bSick]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlay(head_ground)
f = fPlay(sleep)
f = fPlayTime(sleep,5)
f = fPlayReverse(head_ground)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[y\BehaviorSet\bEscaped1]
f = fTrot(0,0)
[y\BehaviorSet\bChasePrey]
f = fTrot(prey,0)
[y\BehaviorSet\bRunFromPredator]
f = fTrot(0,0)
[y\BehaviorSet\bCaughtGuest]
; the time of animation below should match bCaughtBy in guest
f = fPlayTime(stand,4)
[y\BehaviorSet\bCaughtPrey]
f = fDustBall()
f = fPlaySet(bAfterCaught)
[y\BehaviorSet\bAfterCaught]
f = fPlay(stand)
f = fPlayWithSound(roar,roar)
f = fPlay(shake)
f = fPlay(stand)
[y\BehaviorSet\bCaught]
f = fFollow(keeper)
[y\BehaviorSet\bWalk]
f = fWalk(0,0)
[y\BehaviorSet\bRun]
f = fTrot(0,0)
[y\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
[y\BehaviorSet\bHatch]
f = fPlayWithSound(egg_hatch,hatch)
f = fPlay(idle)
f = fHatch()
[y\BehaviorSet\bEatenEgg]
f = fPlayStopAtEnd(egg_break)
[AmbientAnims]
;a = -100
;a = 100
;b = bTest
a = 81
a = 100
b = bHappy
a = 61
a = 80
b = bFriendly
a = 21
a = 60
b = bCalm
a = -20
a = 20
b = bNeutral
a = -60
a = -21
b = bUneasy
a = -80
a = -61
b = bTense
a = -100
a = -81
b = bAngry
;Test BehaviorSet Probabilities to test bHappy1
;[m\BehaviorSet\bHappy]
;f = fPlaySetProb(bHappy1,100)
;Test BehaviorSet Probabilities for testing sound
;[m\BehaviorSet\bSoundtest]
;f = fPlaySetProb(bSoundtest1,100)
[m\BehaviorSet\bSoundtest1]
;f = fPlayWithSound(hunt_roar,huntror)
;f = fPlayWithSound(roar,roar)
;f = fPlayWithSound(roar_up,rorup)
;f = fPlayWithSound(roar_up_left,rorupl)
;f = fPlayWithSound(roar_up_right,rorupr)
;f = fPlayWithSound(roar_down,rordwn)
;f = fPlayWithSound(roar_down_left,rordownl)
;f = fPlayWithSound(roar_down_right,rordownr)
;f = fPlayWithSound(stomp_left,stompl)
;f = fPlayWithSound(stomp_right,stompr)
;f = fPlayWithSound(snort,snort)
;No longer needed
;[m\BehaviorSet\bBaby1]
;f = fPlay(stand)
;f = fWalk(mother,0)
;No longer needed
;[m\BehaviorSet\bBaby2]
;f = fPlay(stand)
;f = fWalk(mother,0)
;No longer needed
;[m\BehaviorSet\bAngryKeeper]
;f = fPlay(stand)
;No longer needed
;[m\BehaviorSet\bBabyActions]
;f = fPlaySetProb(bBaby1,40,bBaby2,40,bIdle1,20)
;plateosaurus does not play with toys
;[m\BehaviorSet\bPlayToys]
;f = fPlaySetProb(bToy3,20,bSpray1,20,bLog1,20,bToy1,20,bToy2,20)
;plateosaurus does not play with toys
;[m\BehaviorSet\bPlayWithToyHappy]
;f = fPlaySetProb(bPlayToys,50,bIdle1,20,bHappy2,20,bNoise1,10)
;plateosaurus does not play with toys
;[m\BehaviorSet\bPlayWithToyFriendly]
;f = fPlaySetProb(bPlayToy,50,bIdle2,30,bNoise2,20)
;Test BehaviorSet Probabilities to test bHappy1
;[y\BehaviorSet\bHappy]
;f = fPlaySetProb(bHappy1,100)
;Test BehaviorSet Probabilities for testing sound
;[y\BehaviorSet\bSoundtest]
;f = fPlaySetProb(bSoundtest1,100)
;No longer needed
;[y\BehaviorSet\bBabyActions]
;f = fPlaySetProb(bBaby1,40,bBaby2,40,bIdle1,20)
;plateosaurus does not play with toys
;[y\BehaviorSet\bPlayToys]
;f = fPlaySetProb(bToy3,20,bSpray1,20,bLog1,20,bToy1,20,bToy2,20)
;plateosaurus does not play with toys
;[y\BehaviorSet\bPlayWithToyHappy]
;f = fPlaySetProb(bPlayToys,50,bIdle1,20,bHappy2,20,bNoise1,10)
;plateosaurus does not play with toys
;[y\BehaviorSet\bPlayWithToyFriendly]
;f = fPlaySetProb(bPlayToy,50,bIdle2,30,bNoise2,20)
;[y\BehaviorSet\bSoundtest1]
;f = fPlayWithSound(hunt_roar,huntror)
;f = fPlayWithSound(roar,roar)
;f = fPlayWithSound(roar_up,rorup)
;f = fPlayWithSound(roar_up_left,rorupl)
;f = fPlayWithSound(roar_up_right,rorupr)
;f = fPlayWithSound(roar_down,rordwn)
;f = fPlayWithSound(roar_down_left,rordownl)
;f = fPlayWithSound(roar_down_right,rordownr)
;f = fPlayWithSound(stomp_left,stompl)
;f = fPlayWithSound(stomp_right,stompr)
;f = fPlayWithSound(snort,snort)
;No longer needed
;[y\BehaviorSet\bBored1]
;f = fPlayTime(stand,10)
;No longer needed
;[y\BehaviorSet\bBored2]
;f = fPlayTime(stand,10)
;f = fPlay(yawn)
;f = fPlayTime(stand,5)
;No longer needed
;[y\BehaviorSet\bAngryKeeper]
;f = fPlay(stand)
;No longer needed
;[y\BehaviorSet\bBaby1]
;f = fPlay(stand)
;f = fWalk(mother,0)
;No longer needed
;[y\BehaviorSet\bBaby2]
;f = fPlay(stand)
;f = fWalk(mother,0)