[Global]
Class = animals
Type = blckbuck
Subtype = 
DefaultSubtype = m
[AnimPath]
m = animals/blckbuck/m
f = animals/blckbuck/f
y = animals/blckbuck/y
[m/Icon]
Icon = animals/blckbuck/icmblckb/icmblckb
[f/Icon]
Icon = animals/blckbuck/icfblckb/icfblckb
[Characteristics/Integers]
[Member]
animals
[f/Characteristics/Strings]
cListImageName = animals/blckbuck/lsfblckb/lsfblckb
cPrefIcon = objects/khejri/SE/SE;Khejri tree 
[m/Characteristics/Strings]
cKeeperFoodType = herbivore
cGeneralInfoTextName = BLCKBUCK_GENERAL
cPlaqueImageName = animals/blckbuck/plblckbu/plblckbu
cListImageName = animals/blckbuck/lsmblckb/lsmblckb
cPrefIcon = objects/khejri/SE/SE;Khejri tree 
[f/Characteristics/Integers]
[m/Characteristics/Integers]
cKeeperFoodType = 0
cPrefIconID = 7076;Khejri tree 
cNameID = 5076; Blackbuck
cHelpID = 5076
cFamily = 5205; Even-toed Ungulate
cGenus = 5132; Antelope
cHabitat = 9400; Savannah
cLocation = 9601; India
cPurchaseCost = 600
cInitialHappiness  = 80
cSlowRate = 20
cMediumRate = 35
cFastRate = 48
cFootprintX = 1
cFootprintY = 1
cCaptivity = 2;//Angry Effects//
cNoFoodChange  = -50
cSickChange = -10	
cOtherAnimalSickChange = -5		
cCrowdHappinessChange = -5
cOtherAnimalAngryChange = 0 
cNumberMinChange = -5
cNumberMaxChange = -5
cAllCrowdedChange = -5
cAngryHabitatChange = -5
cVeryAngryHabitatChange = -30
cDeathChange = -5 
cNoMateChange = 0
cNotEnoughKeepersChange = -10
cAngryTreeChange = -10
cBabyBornChange = 50;//Happy Effects//
cHappyHabitatChange = 20
cMaxHits  = 60
cMinHits  = 0
cPctHits  = 20
cHungerThreshold  = 50
cHungerIncrement  = 3
cNeededFood = 60
cKeeperFoodUnitsEaten = 5
cHungryHealthChange = 10
cSickChance = 5
cEnergyThreshold = 100 
cEnergyIncrement  = 5	
cMaxEnergy = 100
cCrowd = 30
cKeeperFrequency = 2
cSocial = 1
cHabitatSize = 100
cNumberAnimalsMin = 3
cNumberAnimalsMax = 25
cAnimalDensity = 15
cEscapedChange = 30
cPctHabitat = 10
cHabitatPreference = 80
cAnimalDirtyRating = 10
cDirtyIncrement = 2
cDirtyThreshold = 100
cReproductionChance = 2 
cReproductionInterval = 1800
cBabyToAdult = 720
cHappyReproduceThreshold = 95
cOffspring = 1
cMatingType = 0
cIsJumper = 1
cIsClimber = 0
cBashStrength = 0
cFlies = 0
cSwims = 0
cSickTime = 4
cMimic = 70
cMimicHappyDiff = 50
cOtherFood = 0
cFoodTypes = 0
cTreePref = 6
cRockPref = 3
cSpacePref = 50 
cElevationPref = 0
cTimeDeath = 4320
cDeathChance = 10 
cEatVegetationChance = 25
cDrinkWaterChance = 15
cChaseAnimalChance = 25
cClimbsCliffs = 0
cSmallZoodoo = 1
cBuildingUseChance = 10
cAttractiveness = 5
cNeedShelter = 1
[y/Characteristics/Integers]
cChaseAnimalChance = 35
cIsJumper = 0
cAttractiveness = 15
[m/Characteristics/Mixed]
[cCompatibleAnimals]
v = 5200;Primitive ungulates		
v = -5
v = 5201;Racoon			
v = -10
v = 5202;Bears			
v = -20
v = 5203;Primates			
v = -10
v = 5204;Odd-toed ungulates		
v = -5
v = 5205;Even-toed ungulates			
v = -5
v = 5206;Cat			
v = -20
v = 5207;Canine			
v = -20
v = 5208;Hyena			
v = -20
v = 5210;Marsupial		
v = -10
v = 5211;Bird			
v = -5
v = 5212;Seal			
v = -10
v = 5213;Crocodile		
v = -20
v = 5214;Edentate		    	
v = -10
v = 5215;Pig			
v = -10
v = 5216;Dinosaur			
v = -20
v = 5217;Monster			
v = -20
v = 5076;Blackbuck			
v = 5
v = 5039;Penguin			
v = -10
[cSuitableObjects]
v = 9400;Savannah
v = 3
v = 9410;Non-habitat
v = -15
v = 9408;Tundra
v = -25
v = 9402;Deciduos forest
v = -15
v = 9403;Coniferous forest
v = -10
v = 9404;Boreal forest
v = -10
v = 9405;Tropical rainforest
v = -10
v = 9407;Highlands
v = -10
v = 9409;Desert
v = -5
v = 6062;Poo
v = -5
v = 9414;Bad-habitat
v = -100
;FOLIAGE AND ROCKS
v = 7076;Khejri
v = 10
v = 7048;bush1
v = -8
v = 7060;savgrass
v = -8
v = 7000;acacia
v = -8
v = 7001;whtthrna
v = -8
v = 7056;yfacacia
v = -8
v = 7058;umbacac
v = -8
v = 7057;baobab
v = -8
v = 7063;grasstr
v = -8
v = 7030;redgum
v = -8
v = 7019;hquandon
v = 10
v = 7017;eucalyp
v = -8
v = 9200;lrock1
v = 4
v = 9205;srock1
v = 2
v = 9206;srock2
v = 2
;SHELTERS
Leanto1
v = 8104
v = 20
Leanto2
v = 8105
v = 22
Leanto3
v = 8106
v = 26
v = 8107;Conhous1
v = 17
v = 8108;Conhous2
v = 20
v = 8109;Conhous3
v = 22
v = 8110;Wodhous1
v = 17
v = 8111;Wodhous2
v = 20
v = 8112;Wodhous3
v = 22
[cCompatibleTerrain]
v = 16
v = -5
v = 1
v = 95
v = 2
v = -5
v = 4
v = -5
v = 5
v = -5
v = 6
v = -5
v = 8
v = -50
v = 9
v = 5
v = 10
v = -20
v = 11
v = -5
v = 13
v = -5
[m/Animations]
idle = stand
buck = buck
charge = charge
eat = eat
shoo_fly = flyshoo
head_ground = headgrnd
raise_head = headrais
jump_high = highjump
present_horns = horns
jerk = jerk
lie_down = liedown
lie_idle = lieidle
lie_side = lieside
look_left = lookl
look_right = lookr
prance = prance
rearup = rearup
run = run
scratch_back = scrhback
scratch_head = scrhhead
scuff_feet = scuff
stamp_feet = stamp
stand = stand
stand_up = standup
stot = stot
trot = trot
walk = walk
; Box animations
box_idle = objects/box/idle/idle.ani
box_used = objects/box/used/used.ani
[m\BehaviorSet\bHappy]
f = fPlaySetProb(bIdle1,20,bHappy1,50,bHappy2,30)
[m\BehaviorSet\bFriendly]
f = fPlaySetProb(bHappy3,60,bIdle2,40)
[m\BehaviorSet\bCalm]
f = fPlaySetProb(bCalm1,50,bIdle1,30,bIdle2,20)
[m\BehaviorSet\bNeutral]
f = fPlaySetProb(bIdle1,50,bIdle2,50)
[m\BehaviorSet\bUneasy]
f = fPlaySetProb(bUneasy1,60,bIdle2,40)
[m\BehaviorSet\bTense]
f = fPlaySetProb(bTense1,50,bAngry3,20,bIdle3,30)
[m\BehaviorSet\bAngry]
f = fPlaySetProb(bAngry1,50,bAngry2,30,bIdle3,20)
[m\BehaviorSet\bEscaped]
f = fPlaySetProb(bEscaped1,70,bNoise1,30)
[m\BehaviorSet\bBabyActions]
f = fPlaySetProb(Baby1,40,bBaby2,40,bIdle1,20)
[m\BehaviorSet\bPlayToys]
;f = fPlaySetProb(bToy3,20,bSpray1,20,bLog1,20,bToy1,20,bToy2,20)
[m\BehaviorSet\bPlayWithToyHappy]
;f = fPlaySetProb(bPlayToys,50,bIdle1,20,bHappy2,20,bNoise1,10),
[m\BehaviorSet\bPlayWithToyFriendly]
;f = fPlaySetProb(bPlayToy,50,bIdle2,30,bNoise2,20),
[m\BehaviorSet\bIdle1]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,24)
f = fPlayReverse(lie_down)
f = fWalk(0,0)
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,6)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[m\BehaviorSet\bHappy1]
f = fMove(0,0,stot,48)
f = fPlayPingPong(scratch_head)
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlay(eat)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[m\BehaviorSet\bHappy2]
f = fPlay(stand)
f = fPlay(scuff_feet)
f = fWalk(0,0)
f = fPlay(stand)
f = fPlayPingPong(scratch_back)
f = fPlay(stand)
[m\BehaviorSet\bHappy3]
f = fWalk(0,0)
f = fPlay(stand)
f = fPlayPingPong(look_right)
f = fPlay(stand)
f = fPlay(shoo_fly)
f = fMove(0,0,jump_high,58)
f = fPlay(stand)
[m\BehaviorSet\bIdle2]
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,8)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[m\BehaviorSet\bCalm1]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,21)
f = fPlayReverse(lie_down)
f = fWalk(0,0)
f = fPlay(stand)
[m\BehaviorSet\bUneasy1]
f = fPlay(stamp_feet)
f = fMove(0,0,trot,38)
f = fPlay(stand)
f = fPlay(scratch_head)
f = fPlay(head_ground)
f = fPlayTime(eat,10)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[m\BehaviorSet\bTense1]
f = fPlay(stand)
f = fWalk(0,0)
f = fPlay(stamp_feet)
f = fPlay(stand)
f = fPlay(scratch_back)
f = fPlay(stand)
[m\BehaviorSet\bAngry3]
f = fPlay(stand)
f = fPlay(buck)
f = fPlay(stand)
f = fMove(0,0,run,48)
f = fPlay(stand)
f = fPlay(stamp_feet)
f = fPlay(stand)
[m\BehaviorSet\bIdle3]
f = fPlay(stand)
f = fPlayPingPong(raise_head)
f = fPlay(stand)
f = fPlayPingPong(look_left)
f = fPlay(stand)
f = fPlayPingPong(look_right)
f = fPlay(stand)
[m\BehaviorSet\bAngry1]
f = fPlay(stand)
f = fPlay(rearup)
f = fPlay(stand)
f = fPlay(buck)
f = fPlay(stand)
f = fMove(0,0,run,48)
f = fPlay(stamp_feet)
f = fPlay(stand)
[m\BehaviorSet\bAngry2]
f = fMove(0,0,run,48)
f = fPlay(stand)
f = fPlay(rearup)
f = fPlay(stand)
f = fPlayPingPong(present_horns)
f = fPlay(stand)
[m\BehaviorSet\bSleep]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,27)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[m\BehaviorSet\bDie]
f = fPlay(lie_down)
f = fPlayTime(lie_idle,5)
f = fPlayTime(lie_idle,5)
f = fDie()
[m\BehaviorSet\bNoise1]
f = fPlay(stand)
f = fPlay(jerk)
f = fPlay(stand)
[m\BehaviorSet\bNoise2]
f = fPlay(stand)
f = fPlay(stamp_feet)
f = fPlay(stand)
[m\BehaviorSet\bFindKeeperFood]
f = fWalk(keeperfood,0)
f = fPlaySet(bEat)
[m\BehaviorSet\bFindOtherFood]
f = fWalk(otherfood,0)
f = fPlaySet(bEat)
[m\BehaviorSet\bEat]
f = fFaceTowardFood()
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,4)
f = fPlayReverse(head_ground)
f = fPlay(head_ground)
f = fPlayTime(eat,6)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[m\BehaviorSet\bSick]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,27)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[m\BehaviorSet\bEscaped1]
f = fPlay(stand)
f = fMove(0,0,run,48)
f = fPlay(stand)
f = fPlay(buck)
f = fPlay(stand)
[m\BehaviorSet\bChasePrey]
f = fMove(prey,0,"run",48)
[m\BehaviorSet\bRunFromPredator]
f = fRun(0,0)
[m\BehaviorSet\bCaught]
f = fFollow(keeper)
[m\BehaviorSet\bBaby1]
f = fPlay(stand)
f = fWalk(mother,0)
[m\BehaviorSet\bBaby2]
f = fPlay(stand)
f = fWalk(mother,0)
[m\BehaviorSet\bBoard1]
f = fPlayTime(stand,10)
[m\BehaviorSet\bBoard2]
f = fPlayTime(stand,10)
[m\BehaviorSet\bAngryKeeper]
f = fPlay(stand)
[m\BehaviorSet\bWalk]
f = fWalk(0,0)
[m\BehaviorSet\bRun]
f = fMove(0,0,run,48)
[m\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
; Young Blackbuck Behavior Sets
[y\BehaviorSet\bIdle1]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,24)
f = fPlayReverse(lie_down)
f = fMove(0,0,walk,13)
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,6)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[y\BehaviorSet\bHappy1]
f = fPlay(stand)
f = fPlayPingPong(scratch_head)
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlay(eat)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[y\BehaviorSet\bHappy2]
f = fPlay(stand)
f = fPlay(scuff_feet)
f = fMove(0,0,walk,13)
f = fPlay(stand)
f = fPlayPingPong(scratch_back)
f = fPlay(stand)
[y\BehaviorSet\bHappy3]
f = fMove(0,0,walk,13)
f = fPlay(stand)
f = fPlayPingPong(look_right)
f = fPlay(stand)
f = fPlay(shoo_fly)
f = fMove(0,0,jump_high,32)
f = fPlay(stand)
[y\BehaviorSet\bIdle2]
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,8)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[y\BehaviorSet\bCalm1]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,21)
f = fPlayReverse(lie_down)
f = fMove(0,0,walk,13)
f = fPlay(stand)
[y\BehaviorSet\bUneasy1]
f = fPlay(stamp_feet)
f = fMove(0,0,trot,32)
f = fPlay(stand)
f = fPlay(scratch_head)
f = fPlay(head_ground)
f = fPlayTime(eat,10)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[y\BehaviorSet\bTense1]
f = fPlay(stand)
f = fMove(0,0,walk,13)
f = fPlay(stamp_feet)
f = fPlay(stand)
f = fPlay(scratch_back)
f = fPlay(stand)
[y\BehaviorSet\bAngry3]
f = fPlay(stand)
f = fPlay(buck)
f = fPlay(stand)
f = fMove(0,0,run,32)
f = fPlay(stand)
f = fPlay(stamp_feet)
f = fPlay(stand)
[y\BehaviorSet\bIdle3]
f = fPlay(stand)
f = fPlayPingPong(raise_head)
f = fPlay(stand)
f = fPlayPingPong(look_left)
f = fPlay(stand)
f = fPlayPingPong(look_right)
f = fPlay(stand)
[y\BehaviorSet\bAngry1]
f = fPlay(stand)
f = fPlay(rearup)
f = fPlay(stand)
f = fPlay(buck)
f = fPlay(stand)
f = fMove(0,0,run,32)
f = fPlay(stamp_feet)
f = fPlay(stand)
[y\BehaviorSet\bAngry2]
f = fMove(0,0,run,32)
f = fPlay(stand)
f = fPlay(rearup)
f = fPlay(stand)
f = fPlayPingPong(present_horns)
f = fPlay(stand)
[y\BehaviorSet\bSleep]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,27)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[y\BehaviorSet\bDie]
f = fPlay(lie_down)
f = fPlayTime(lie_idle,5)
f = fPlayTime(lie_idle,5)
f = fDie()
[y\BehaviorSet\bNoise1]
f = fPlay(stand)
f = fPlay(jerk)
f = fPlay(stand)
[y\BehaviorSet\bNoise2]
f = fPlay(stand)
f = fPlay(stamp_feet)
f = fPlay(stand)
[y\BehaviorSet\bFindKeeperFood]
f = fWalk(keeperfood,0)
f = fPlaySet(bEat)
[y\BehaviorSet\bFindOtherFood]
f = fWalk(otherfood,0)
f = fPlaySet(bEat)
[y\BehaviorSet\bEat]
f = fFaceTowardFood()
f = fPlay(stand)
f = fPlay(head_ground)
f = fPlayTime(eat,4)
f = fPlayReverse(head_ground)
f = fPlay(head_ground)
f = fPlayTime(eat,6)
f = fPlayReverse(head_ground)
f = fPlay(stand)
[y\BehaviorSet\bSick]
f = fPlay(stand)
f = fPlay(lie_down)
f = fPlayTime(lie_idle,27)
f = fPlayReverse(lie_down)
f = fPlay(stand)
[y\BehaviorSet\bEscaped1]
f = fPlay(stand)
f = fMove(0,0,run,32)
f = fPlay(stand)
f = fPlay(buck)
f = fPlay(stand)
[y\BehaviorSet\bChasePrey]
f = fMove(prey,0,"run",32)
[y\BehaviorSet\bRunFromPredator]
f = fRun(0,0)
[y\BehaviorSet\bCaught]
f = fFollow(keeper)
[y\BehaviorSet\bBaby1]
f = fPlay(stand)
f = fWalk(mother,0)
[y\BehaviorSet\bBaby2]
f = fPlay(stand)
f = fWalk(mother,0)
[y\BehaviorSet\bBoard1]
f = fPlayTime(stand,10)
[y\BehaviorSet\bBoard2]
f = fPlayTime(stand,10)
[y\BehaviorSet\bAngryKeeper]
f = fPlay(stand)
[y\BehaviorSet\bWalk]
f = fMove(0,0,walk,13)
[y\BehaviorSet\bRun]
f = fMove(0,0,run,32)
[y\BehaviorSet\bRattle]
f = fPlayWithSound(box_used,placesound)
[AmbientAnims]
a = 81
a = 100
b = bHappy
a = 61
a = 80
b = bFriendly
a = 21
a = 60
b = bCalm
a = -20
a = 20
b = bNeutral
a = -60
a = -21
b = bUneasy
a = -80
a = -61
b = bTense
a = -100
a = -81
b = bAngry
