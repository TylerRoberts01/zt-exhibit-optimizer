TERRAIN = {0 : 'grass',\
           1 : 'savannah grass',\
           2 : 'sand',\
           3 : 'dirt',\
           4 : 'rainforest floor',\
           5 : 'brown rock',\
           6 : 'gray rock',\
           7 : 'gravel',\
           8 : 'snow',\
           9 : 'fresh water',\
           10: 'salt water',\
           11: 'deciduous floor',\
           12: 'waterfall',\
           13: 'coniferous floor',\
           14: 'concrete',\
           15: 'asphalt',\
           16: 'trampled'}

class H:
    GLOBAL = '[Global]\n'
    ANIM_PATH = '[AnimPath]\n'
    ICON = '[m/Icon]\n'
    CHAR_INT = '[Characteristics/Integers]\n'
    MEMBER = '[Member]\n'
    FCHAR_STR = '[f/Characteristics/Strings]\n'
    MCHAR_STR = '[m/Characteristics/Strings]\n'
    FCHAR_INT = '[f/Characteristics/Integers]\n'
    MCHAR_INT = '[m/Characteristics/Integers]\n'
    YCHAR_INT = '[y/Characteristics/Integers]\n'
    MCHAR_MIX = '[m/Characteristics/Mixed]\n'
    COMP_ANIMALS = '[cCompatibleAnimals]\n'
    SUIT_OBJ = '[cSuitableObjects]\n'
    FOL_ROCKS = ';FOLIAGE AND ROCKS\n'
    SHELTERS = ';SHELTERS\n'
    COMP_TERRAIN = '[cCompatibleTerrain]\n'
    FX = '[m/Animations]\n'
    FFX = '[f/Animations]\n'

HEADERS = [H.GLOBAL, H.ANIM_PATH, H.ICON, H.CHAR_INT, H.MEMBER, H.FCHAR_STR, H.MCHAR_STR,\
           H.FCHAR_INT, H.MCHAR_INT, H.YCHAR_INT, H.MCHAR_MIX, H.COMP_ANIMALS, H.SUIT_OBJ,\
           H.FOL_ROCKS, H.SHELTERS, H.COMP_TERRAIN, H.FX, H.FFX]

STR = 'str'
INT = 'int'

def split_file(file):
    result = {}
    lines = file.readlines()
    current_header = ''

    # add all headers
    for h in HEADERS:
        result[h] = [h]

    for l in lines:
        if l in HEADERS:
            current_header = l
        elif current_header in HEADERS:
            result[current_header].append(l)
    return result

def find_line(start, lines):
    for i in range(len(lines)):
        if lines[i].startswith(start):
            return i
    return -1