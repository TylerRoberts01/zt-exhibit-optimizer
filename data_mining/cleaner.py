import os
from utils import HEADERS
from utils import split_file
from utils import H

INPUT_DIR = 'animals'
OUTPUT_DIR = 'animals_clean'

HEADERS_TO_CLEAN = [H.FCHAR_STR, H.MCHAR_STR, H.FCHAR_INT, H.MCHAR_INT, H.MCHAR_MIX, H.COMP_ANIMALS,\
                    H.SUIT_OBJ, H.SHELTERS]

DEL = [H.COMP_TERRAIN]

# do not write blank lines
def try_write(outfile, line):
    if not line == '\n':
        outfile.write(line)

def clean_comments(lines, outfile):
    # foliage and rocks manual cleaning
    if lines[0] == H.FOL_ROCKS:
        add_on = ''
        outfile.write(lines[0])
        for i in range(1, len(lines)):
            l = lines[i]
            if l[0].isalpha() and l[0] != 'v':
                add_on = ';' + l
            elif 'SHELTERS AND TOYS' in l:
                add_on = ''
            elif l.startswith(':'):
                add_on = ';' + l[1:]
            elif l.startswith(';'):
                add_on = l
            else:
                try_write(outfile, l[:-1] + add_on[:-1] + '\n')
                add_on = ''

    # move comments to the same line as the value
    elif lines[0] in HEADERS_TO_CLEAN:
        add_on = ''
        outfile.write(lines[0])
        for i in range(1, len(lines)):
            l = lines[i]
            if l.startswith(';'):
                add_on = l
            else:
                try_write(outfile, l[:-1] + add_on[:-1] + '\n')
                add_on = ''
    # delete comments (not necessary to understand values)
    elif lines[0] in DEL:
        for l in lines:
            if not l.startswith(';'): try_write(outfile, l)
    # not necessary to clean
    else:
        for l in lines:
            try_write(outfile, l)

def clean():
    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)

    for filename in os.listdir(INPUT_DIR):
        f = open(f'{INPUT_DIR}/{filename}')
        lines = split_file(f)

        outfile = open(OUTPUT_DIR + "/" + filename[:-3] + ".txt", "w")

        for h in HEADERS[:-1]:
            if h in lines:
                clean_comments(lines[h], outfile)

        outfile.close()