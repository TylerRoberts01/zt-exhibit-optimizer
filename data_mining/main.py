import cleaner
import decoder
import final_cleaner

def main():
    cleaner.clean()
    decoder.decode()
    final_cleaner.clean()

if __name__ == '__main__':
    main()