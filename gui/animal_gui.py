import PySimpleGUI as sg

STR = 'str'
INT = 'int'

def animal_gui(props):
    layout = [
        [sg.Text(k), sg.Text(props[k])] for k in props
    ]
