import PySimpleGUI as sg
import json
from animal_gui import animal_gui

WINDOW_NAME = 'ZT Exhibit Optimizer'

json_file = open('../data/animals.json')
animal_props = json.load(json_file)
json_file.close()

names = [a for a in animal_props.keys()]
current_props = animal_props[next(iter(animal_props))]

def filter_names(search):
    search = search.lower()
    filtered = [x for x in names if search in x.lower()]
    filtered.sort(key = lambda x: x.lower().find(search[0]))
    return filtered

def update_props(window, props):
    for p in props:
        if p in ['foliage_and_rocks', 'terrain']:
            text = f'{p}:\n'
            for item in props[p]:
                text += f'{item[0]}: {item[1]}' + ('%' if p == 'terrain' else '') + '\n'
            window.Element(f'_{p}_').Update(text)
        else:
            window.Element(f'_{p}_').Update(f'{p}: {props[p]}')

layout = [  [sg.Text(WINDOW_NAME)],
            [sg.Input(do_not_clear=True, size=(20,1),enable_events=True, key='_INPUT_')],
            [sg.Listbox(names, size=(20,4), enable_events=True, key='_LIST_')],
            [sg.Button('Submit', visible=False, bind_return_key=True)],
            [
                sg.Column(([
                    [sg.Text('', key=f'_{prop}_')]
                    for prop in current_props
                    if prop != 'foliage_and_rocks' and prop != 'terrain'
                ])),

                sg.Column([
                    [sg.Text('', key=f'_{prop}_')]
                    for prop in current_props
                    if prop == 'foliage_and_rocks'
                ]),
                
                sg.Column([
                    [sg.Text('', key=f'_{prop}_')]
                    for prop in current_props
                    if prop == 'terrain'
                ])
            ]
        ]

window = sg.Window(WINDOW_NAME).Layout(layout)
window.Read()
update_props(window, current_props)

started = False

# Event Loop
while True:
    event, values = window.Read()
    filtered_names = names
    # always check for closed window
    if event is None or event == 'Exit':
        break
    # if a keystroke entered in search field, update list
    if values['_INPUT_'] != '':
        search = values['_INPUT_']
        filtered_names = filter_names(search)
        window.Element('_LIST_').Update(filtered_names)
    # no input, display original list
    else:
        window.Element('_LIST_').Update(names)
    # if a list item is chosen, update current animal
    if event == '_LIST_' and len(values['_LIST_']):
        update_props(window, animal_props[values['_LIST_'][0]])
    # pressed enter, choose animal highest on the list
    if event == 'Submit':
        update_props(window, animal_props[filtered_names[0]])

window.close()